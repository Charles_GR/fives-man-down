package com.charles.fives_man_down.processing.media;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.Base64;
import android.widget.ImageView;
import java.io.*;

public abstract class ImageParser
{

    public static String encodeImage(ImageView imageView)
    {
        BitmapDrawable bitmapDrawable = (BitmapDrawable)imageView.getDrawable();
        if(bitmapDrawable == null)
        {
            return null;
        }
        Bitmap bitmap = bitmapDrawable.getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        return Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
    }

    public static Bitmap decodeImage(String encodedImage)
    {
        byte[] imageBytes = Base64.decode(encodedImage, 0);
        return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
    }

}
