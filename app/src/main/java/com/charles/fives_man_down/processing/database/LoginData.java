package com.charles.fives_man_down.processing.database;

import android.support.annotation.NonNull;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.MyApplication;
import com.charles.fives_man_down.model.database.User;
import com.charles.fives_man_down.model.runnable.SingleArgRunnable;
import com.charles.fives_man_down.view.views.main.LoginActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.*;

public abstract class LoginData
{

    private static String username = "";
    private static String email = "";

    public static String getUsername()
    {
        return username;
    }

    public static String getEmail()
    {
        return email;
    }

    public static void setUsername(String username)
    {
        LoginData.username = username;
    }

    public static void setEmail(String email)
    {
        LoginData.email = email;
    }

    public static void login(final String email, final String password, final SingleArgRunnable<String> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password).addOnCompleteListener(LoginActivity.getInstance(), new OnCompleteListener<AuthResult>()
        {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task)
            {
                if(task.isSuccessful())
                {
                    if(FirebaseAuth.getInstance().getCurrentUser().isEmailVerified())
                    {
                        onSuccess.run(email);
                    }
                    else
                    {
                        FirebaseAuth.getInstance().getCurrentUser().sendEmailVerification();
                        FirebaseAuth.getInstance().signOut();
                        onError.run(MyApplication.getInstance().getResources().getString(R.string.email_not_verified));
                    }
                }
                else if(onError != null)
                {
                    onError.run(task.getException().getMessage());
                }
            }
        });
    }

    public static void register(final String username, final String email, final String password, final Runnable onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password).addOnCompleteListener(LoginActivity.getInstance(), new OnCompleteListener<AuthResult>()
        {
            @Override
            public void onComplete(Task<AuthResult> task)
            {
                if(task.isSuccessful())
                {
                    FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>()
                    {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task)
                        {
                            if(task.isSuccessful())
                            {
                                FirebaseData.getUserRef(username).getRef().addListenerForSingleValueEvent(new ValueEventListener()
                                {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot)
                                    {
                                        if(dataSnapshot.exists())
                                        {
                                            FirebaseAuth.getInstance().getCurrentUser().delete().addOnCompleteListener(LoginActivity.getInstance(), new OnCompleteListener<Void>()
                                            {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task)
                                                {
                                                    if(task.isSuccessful())
                                                    {
                                                        onError.run(MyApplication.getInstance().getResources().getString(R.string.username_in_use));
                                                    }
                                                }
                                            });
                                        }
                                        else
                                        {
                                            FirebaseData.getUserRef(username).setValue(new User(username, email)).addOnCompleteListener(new OnCompleteListener<Void>()
                                            {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task)
                                                {
                                                    if(task.isSuccessful())
                                                    {
                                                        FirebaseAuth.getInstance().getCurrentUser().sendEmailVerification();
                                                        onSuccess.run();
                                                    }
                                                    else
                                                    {
                                                        onError.run(task.getException().getMessage());
                                                    }
                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError)
                                    {
                                        onError.run(databaseError.getMessage());
                                    }
                                });
                            }
                            else
                            {
                                onError.run(task.getException().getMessage());
                            }
                        }
                    });
                }
                else
                {
                    onError.run(task.getException().getMessage());
                }
            }
        });
    }

    public static void getUsernameFromEmail(final String email, final SingleArgRunnable<String> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.usersRef.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    User user = child.getValue(User.class);
                    if(email.equals(user.getEmail()))
                    {
                        onSuccess.run(user.getUsername());
                        return;
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                onError.run(databaseError.getMessage());
            }
        });
    }

    public static void resetPassword(final String email, final Runnable onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseAuth.getInstance().sendPasswordResetEmail(email).addOnCompleteListener(LoginActivity.getInstance(), new OnCompleteListener<Void>()
        {
            @Override
            public void onComplete(@NonNull Task<Void> task)
            {
                if(task.isSuccessful())
                {
                    onSuccess.run();
                }
                else
                {
                    onError.run(task.getException().getMessage());
                }
            }
        });
    }

}