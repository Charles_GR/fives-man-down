package com.charles.fives_man_down.processing;

import android.Manifest;
import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.*;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.NotificationCompat;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.MyApplication;
import com.charles.fives_man_down.model.database.Notification;
import com.charles.fives_man_down.model.database.Request;
import com.charles.fives_man_down.processing.database.FirebaseData;
import com.charles.fives_man_down.processing.database.LoginData;
import com.charles.fives_man_down.view.views.requests.RequestsActivity;
import com.google.firebase.database.*;
import com.google.gson.Gson;

public class NotificationService extends Service implements LocationListener
{

    private static NotificationService instance;

    public NotificationService getInstance()
    {
        return instance;
    }

    @Override
    public void onCreate()
    {
        instance = this;
        checkLocationEnabled();
        FirebaseData.getUserNotificationsRef(LoginData.getUsername()).addChildEventListener(new ChildEventListener()
        {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s)
            {
                Notification notification = dataSnapshot.getValue(Notification.class);
                dataSnapshot.getRef().removeValue();
                switch(notification.getTitle())
                {
                    case Notification.NEW_PLAYER_REQUEST:
                        if(!MyApplication.getInstance().getOptions().getNotifyOnNewRequestCreated())
                        {
                            return;
                        }
                        Request request = dataSnapshot.child("request").getValue(Request.class);
                        Double distance = request.calcDistanceToPosition();
                        if(distance == null || distance <= MyApplication.getInstance().getOptions().getNewRequestMaxDistanceToNotify())
                        {
                            showNotification(NotificationService.this, notification);
                        }
                        break;
                    case Notification.PLAYER_REQUEST_ACCEPTED:
                        if(MyApplication.getInstance().getOptions().getNotifyOnMyRequestAccepted())
                        {
                            showNotification(NotificationService.this, notification);
                        }
                        break;
                    case Notification.PLAYER_REQUEST_UNACCEPTED:
                        if(MyApplication.getInstance().getOptions().getNotifyOnMyRequestUnaccepted())
                        {
                            showNotification(NotificationService.this, notification);
                        }
                        break;
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s)
            {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot)
            {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s)
            {

            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    @Override
    public void onLocationChanged(Location location)
    {
        MyApplication.getInstance().setUserPosition(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras)
    {

    }

    @Override
    public void onProviderEnabled(String provider)
    {

    }

    @Override
    public void onProviderDisabled(String provider)
    {

    }

    private void showNotification(Context context, Notification notification)
    {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));
        builder.setContentTitle(notification.getTitle());
        builder.setDefaults(NotificationCompat.DEFAULT_ALL);
        builder.setContentText(notification.createMessage());
        builder.setAutoCancel(true);
        if(notification.createBigMessage() != null)
        {
            builder.setStyle(new NotificationCompat.BigTextStyle().bigText(notification.createBigMessage()));
        }
        Intent intent = new Intent(this, RequestsActivity.class);
        intent.putExtra("FromNotification", true);
        intent.putExtra("NotificationTitle", notification.getTitle());
        intent.putExtra("Request", new Gson().toJson(notification.getRequest(), Request.class));
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, builder.build());
    }

    @SuppressWarnings("ResourceType")
    private void initialiseLocation()
    {
        LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if(isGPSEnabled || isNetworkEnabled)
        {
            String provider = isGPSEnabled ? LocationManager.GPS_PROVIDER : LocationManager.NETWORK_PROVIDER;
            locationManager.requestLocationUpdates(provider, 20000, 100, this);
            MyApplication.getInstance().setUserPosition(locationManager.getLastKnownLocation(provider));
        }
    }

    private void checkLocationEnabled()
    {
        LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            initialiseLocation();
        }
    }

}
