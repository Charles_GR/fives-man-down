package com.charles.fives_man_down.processing.database;

import com.charles.fives_man_down.model.database.*;
import com.google.firebase.database.*;

public abstract class NotificationsData
{

    public static void notifyRequestCreated(final Request request)
    {
        FirebaseData.usersRef.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    User user = child.getValue(User.class);
                    if(!user.getUsername().equals(request.getRequester()))
                    {
                        Notification notification = new Notification(Notification.NEW_PLAYER_REQUEST, request, request.getRequester());
                        FirebaseData.getUserNotificationsRef(user.getUsername()).push().setValue(notification);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
    }

    public static void notifyRequestAccepted(final Request request)
    {
        Notification notification = new Notification(Notification.PLAYER_REQUEST_ACCEPTED, request, LoginData.getUsername());
        FirebaseData.getUserNotificationsRef(request.getRequester()).push().setValue(notification);
    }

    public static void notifyRequestUnaccepted(final Request request)
    {
        Notification notification = new Notification(Notification.PLAYER_REQUEST_UNACCEPTED, request, LoginData.getUsername());
        FirebaseData.getUserNotificationsRef(request.getRequester()).push().setValue(notification);
    }

}
