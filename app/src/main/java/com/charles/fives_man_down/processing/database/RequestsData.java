package com.charles.fives_man_down.processing.database;

import android.support.annotation.NonNull;
import com.charles.fives_man_down.model.database.*;
import com.charles.fives_man_down.model.runnable.ListRunnable;
import com.charles.fives_man_down.model.runnable.SingleArgRunnable;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.*;
import com.google.firebase.database.DatabaseReference.CompletionListener;
import com.google.firebase.database.Transaction.Handler;
import com.google.firebase.database.Transaction.Result;
import java.util.*;

public abstract class RequestsData
{

    public static void getMyRequests(final ListRunnable<Request> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getRequestsRef().addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                List<Request> requests = new ArrayList<>();
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    Request request = child.getValue(Request.class);
                    if(!request.hasExpired() && request.getRequester().equals(LoginData.getUsername()))
                    {
                        requests.add(request);
                    }
                }
                onSuccess.run(requests);
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                onError.run(databaseError.getMessage());
            }
        });
    }

    public static void getOpenRequests(final ListRunnable<Request> onSuccess, final SingleArgRunnable<String> onError)
    {
        ProfilesData.getMyProfile(new SingleArgRunnable<Profile>()
        {
            @Override
            public void run(final Profile profile)
            {
                FirebaseData.getRequestsRef().addValueEventListener(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        List<Request> requests = new ArrayList<>();
                        for(DataSnapshot child : dataSnapshot.getChildren())
                        {
                            Request request = child.getValue(Request.class);
                            if(!request.hasExpired() && request.getPlayersNeeded() > 0 && !request.getRequester().equals(LoginData.getUsername()) &&
                                    !request.isAcceptedBy(LoginData.getUsername()) && request.profileCanAcceptRequest(profile))
                            {
                                requests.add(request);
                            }
                        }
                        onSuccess.run(requests);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {
                        onError.run(databaseError.getMessage());
                    }
                });
            }
        },
        new SingleArgRunnable<String>()
        {
            @Override
            public void run(String text)
            {
                onError.run(text);
            }
        });
    }

    public static void getAcceptedRequests(final ListRunnable<Request> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getRequestsRef().addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                List<Request> requests = new ArrayList<>();
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    Request request = child.getValue(Request.class);
                    if(!request.hasExpired() && request.isAcceptedBy(LoginData.getUsername()))
                    {
                        requests.add(request);
                    }
                }
                onSuccess.run(requests);
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                onError.run(databaseError.getMessage());
            }
        });
    }

    public static void saveRequest(String description, int playersNeeded, String minExperienceLevel, String maxExperienceLevel, Position position, Date startTime,
                                   final Runnable onSuccess, final SingleArgRunnable<String> onError)
    {
        DatabaseReference databaseReference = FirebaseData.getRequestsRef().push();
        final Request request = new Request(databaseReference.getKey(), description, playersNeeded, minExperienceLevel, maxExperienceLevel, position, startTime, LoginData.getUsername());
        databaseReference.setValue(request).addOnCompleteListener(new OnCompleteListener<Void>()
        {
            @Override
            public void onComplete(@NonNull Task<Void> task)
            {
                if(task.isSuccessful())
                {
                    NotificationsData.notifyRequestCreated(request);
                    onSuccess.run();
                }
                else
                {
                    onError.run(task.getException().getMessage());
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    public static void acceptRequest(String key, final Runnable onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getRequestRef(key).runTransaction(new Handler()
        {
            @Override
            public Result doTransaction(MutableData mutableData)
            {
                int newPlayersNeeded = mutableData.child("playersNeeded").getValue(Integer.class) - 1;
                mutableData.child("playersNeeded").setValue(Math.max(newPlayersNeeded, 0));
                boolean accepteesExists = mutableData.child("acceptees").getValue(List.class) != null;
                List<String> acceptees = accepteesExists ? mutableData.child("acceptees").getValue(List.class) : new ArrayList<String>();
                acceptees.add(LoginData.getUsername());
                mutableData.child("acceptees").setValue(acceptees);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot)
            {
                if(databaseError == null)
                {
                    NotificationsData.notifyRequestAccepted(dataSnapshot.getValue(Request.class));
                    onSuccess.run();
                }
                else
                {
                    onError.run(databaseError.getMessage());
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    public static void unacceptRequest(String key, final Runnable onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getRequestRef(key).runTransaction(new Handler()
        {
            @Override
            public Result doTransaction(MutableData mutableData)
            {
                mutableData.child("playersNeeded").setValue(mutableData.child("playersNeeded").getValue(Integer.class) + 1);
                GenericTypeIndicator<List<String>> genericTypeIndicator = new GenericTypeIndicator<List<String>>(){};
                List<String> acceptees = mutableData.child("acceptees").getValue(genericTypeIndicator);
                acceptees.remove(LoginData.getUsername());
                mutableData.child("acceptees").setValue(acceptees);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot)
            {
                if(databaseError == null)
                {
                    NotificationsData.notifyRequestUnaccepted(dataSnapshot.getValue(Request.class));
                    onSuccess.run();
                }
                else
                {
                    onError.run(databaseError.getMessage());
                }
            }
        });
    }

    public static void deleteRequest(String key, final Runnable onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getRequestRef(key).removeValue(new CompletionListener()
        {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference)
            {
                if(databaseError == null)
                {
                    onSuccess.run();
                }
                else
                {
                    onError.run(databaseError.getMessage());
                }
            }
        });
    }

}
