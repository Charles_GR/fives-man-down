package com.charles.fives_man_down.processing.database;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public abstract class FirebaseData
{

    public static final DatabaseReference baseRef = FirebaseDatabase.getInstance().getReference();
    public static final DatabaseReference usersRef = baseRef.child("users");
    public static final DatabaseReference optionsRef = baseRef.child("options");

    public static DatabaseReference getUserRef(String username)
    {
        return usersRef.child(username);
    }

    public static DatabaseReference getUserOptionsRef(String username)
    {
        return optionsRef.child(username);
    }

    public static DatabaseReference getNotifyOnNewRequestCreatedRef(String username)
    {
        return getUserOptionsRef(username).child("notifyOnNewRequestCreated");
    }

    public static DatabaseReference getNotifyOnMyRequestAcceptedRef(String username)
    {
        return getUserOptionsRef(username).child("notifyOnMyRequestAccepted");
    }

    public static DatabaseReference getNotifyOnMyRequestUnacceptedRef(String username)
    {
        return getUserOptionsRef(username).child("notifyOnMyRequestUnaccepted");
    }

    public static DatabaseReference getNewRequestMaxDistanceToNotifyRef(String username)
    {
        return getUserOptionsRef(username).child("newRequestMaxDistanceToNotify");
    }

    public static DatabaseReference getProfilesRef()
    {
        return baseRef.child("profiles");
    }

    public static DatabaseReference getProfileRef(String username)
    {
        return getProfilesRef().child(username);
    }

    public static DatabaseReference getProfileFeedbackRef(String username)
    {
        return getProfileRef(username).child("feedback");
    }

    public static DatabaseReference getRequestsRef()
    {
        return baseRef.child("requests");
    }

    public static DatabaseReference getRequestRef(String key)
    {
        return getRequestsRef().child(key);
    }

    public static DatabaseReference getNotificationsRef()
    {
        return baseRef.child("notifications");
    }

    public static DatabaseReference getUserNotificationsRef(String username)
    {
        return getNotificationsRef().child(username);
    }

}

