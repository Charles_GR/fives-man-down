package com.charles.fives_man_down.processing.database;

import android.support.annotation.NonNull;

import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.MyApplication;
import com.charles.fives_man_down.model.database.*;
import com.charles.fives_man_down.model.runnable.ListRunnable;
import com.charles.fives_man_down.model.runnable.SingleArgRunnable;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.*;
import com.google.firebase.database.Transaction.Handler;
import com.google.firebase.database.Transaction.Result;
import java.util.ArrayList;
import java.util.List;

public abstract class ProfilesData
{

    public static void loadMyProfile(final boolean reload, final SingleArgRunnable<Profile> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getProfileRef(LoginData.getUsername()).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                Profile profile = dataSnapshot.getValue(Profile.class);
                if(profile == null && reload)
                {
                    onError.run(MyApplication.getInstance().getResources().getString(R.string.your_profile_not_exist));
                }
                else
                {
                    onSuccess.run(profile);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                onError.run(databaseError.getMessage());
            }
        });
    }

    public static void saveMyProfile(final String fullName, final String bio, final String photo, final String experienceLevel, final Runnable onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getProfileRef(LoginData.getUsername()).runTransaction(new Handler()
        {
            @Override
            public Result doTransaction(MutableData mutableData)
            {
                mutableData.child("username").setValue(LoginData.getUsername());
                mutableData.child("fullName").setValue(fullName);
                mutableData.child("bio").setValue(bio);
                mutableData.child("photo").setValue(photo);
                mutableData.child("experienceLevel").setValue(experienceLevel);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot)
            {
                if(databaseError == null)
                {
                    onSuccess.run();
                }
                else
                {
                    onError.run(databaseError.getMessage());
                }
            }

        });
    }

    public static void getProfiles(final ListRunnable<Profile> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getProfilesRef().addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                List<Profile> profiles = new ArrayList<>();
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    if(!child.getKey().equals(LoginData.getUsername()))
                    {
                        profiles.add(child.getValue(Profile.class));
                    }
                }
                onSuccess.run(profiles);
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                onError.run(databaseError.getMessage());
            }
        });
    }

    public static void getMyProfile(final SingleArgRunnable<Profile> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getProfileRef(LoginData.getUsername()).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                Profile profile = dataSnapshot.getValue(Profile.class);
                onSuccess.run(profile);
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                onError.run(databaseError.getMessage());
            }
        });
    }

    public static void getProfileFeedback(String username, final ListRunnable<Feedback> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getProfileFeedbackRef(username).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                List<Feedback> feedback = new ArrayList<>();
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    feedback.add(child.getValue(Feedback.class));
                }
                onSuccess.run(feedback);
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                onError.run(databaseError.getMessage());
            }
        });
    }

    public static void saveProfileFeedback(String username, String rating, String perceivedExperienceLevel, String text, final Runnable onSuccess, final SingleArgRunnable<String> onError)
    {
        final Feedback feedback = new Feedback(rating, perceivedExperienceLevel, text, username, LoginData.getUsername());
        FirebaseData.getProfileFeedbackRef(username).child(LoginData.getUsername()).setValue(feedback).addOnCompleteListener(new OnCompleteListener<Void>()
        {
            @Override
            public void onComplete(@NonNull Task<Void> task)
            {
                if(task.isSuccessful())
                {
                    onSuccess.run();
                }
                else
                {
                    onError.run(task.getException().getMessage());
                }
            }
        });
    }

}