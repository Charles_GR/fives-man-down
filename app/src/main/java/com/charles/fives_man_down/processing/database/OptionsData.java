package com.charles.fives_man_down.processing.database;

import android.support.annotation.NonNull;
import android.util.Log;
import com.charles.fives_man_down.model.MyApplication;
import com.charles.fives_man_down.model.database.Options;
import com.charles.fives_man_down.model.runnable.SingleArgRunnable;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.*;

public abstract class OptionsData
{

    public static void getOptions(final SingleArgRunnable<Options> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.optionsRef.child(LoginData.getUsername()).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                onSuccess.run(dataSnapshot.getValue(Options.class));
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                onError.run(databaseError.getMessage());
            }
        });
    }

    public static void setNotifyOnNewRequestCreatedValue(final boolean value, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getNotifyOnNewRequestCreatedRef(LoginData.getUsername()).setValue(value).addOnCompleteListener(new OnCompleteListener<Void>()
        {
            @Override
            public void onComplete(@NonNull Task<Void> task)
            {
                if(task.isSuccessful())
                {
                    MyApplication.getInstance().getOptions().setNotifyOnNewRequestCreated(value);
                }
                else
                {
                    onError.run(task.getException().getMessage());
                }
            }
        });
    }

    public static void setNotifyOnMyRequestAcceptedValue(final boolean value, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getNotifyOnMyRequestAcceptedRef(LoginData.getUsername()).setValue(value).addOnCompleteListener(new OnCompleteListener<Void>()
        {
            @Override
            public void onComplete(@NonNull Task<Void> task)
            {
                if(task.isSuccessful())
                {
                    MyApplication.getInstance().getOptions().setNotifyOnMyRequestAccepted(value);
                }
                else
                {
                    onError.run(task.getException().getMessage());
                }
            }
        });
    }

    public static void setNotifyOnMyRequestUnacceptedValue(final boolean value, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getNotifyOnMyRequestUnacceptedRef(LoginData.getUsername()).setValue(value).addOnCompleteListener(new OnCompleteListener<Void>()
        {
            @Override
            public void onComplete(@NonNull Task<Void> task)
            {
                if(task.isSuccessful())
                {
                    MyApplication.getInstance().getOptions().setNotifyOnMyRequestUnaccepted(value);
                }
                else
                {
                    onError.run(task.getException().getMessage());
                }
            }
        });
    }

    public static void setNewRequestMaxDistanceValue(final int value, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getNewRequestMaxDistanceToNotifyRef(LoginData.getUsername()).setValue(value).addOnCompleteListener(new OnCompleteListener<Void>()
        {
            @Override
            public void onComplete(@NonNull Task<Void> task)
            {
                if(task.isSuccessful())
                {
                    MyApplication.getInstance().getOptions().setNewRequestMaxDistanceToNotify(value);
                }
                else
                {
                    onError.run(task.getException().getMessage());
                }
            }
        });
    }

}
