package com.charles.fives_man_down.processing.database;

import android.support.annotation.NonNull;
import com.charles.fives_man_down.model.runnable.SingleArgRunnable;
import com.charles.fives_man_down.view.views.account.AccountActivity;
import com.charles.fives_man_down.view.views.main.LoginActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.*;
import com.google.firebase.database.*;

public abstract class AccountData
{

    public static void changeEmail(final String password, final String newEmail, final Runnable onSuccess, final SingleArgRunnable<String> onError)
    {
        AuthCredential credential = EmailAuthProvider.getCredential(LoginData.getEmail(), password);
        FirebaseAuth.getInstance().getCurrentUser().reauthenticate(credential).addOnCompleteListener(AccountActivity.getInstance(), new OnCompleteListener<Void>()
        {
            @Override
            public void onComplete(@NonNull Task<Void> task)
            {
                if(task.isSuccessful())
                {
                    FirebaseData.usersRef.addListenerForSingleValueEvent(new ValueEventListener()
                    {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot)
                        {
                            FirebaseAuth.getInstance().getCurrentUser().updateEmail(newEmail).addOnCompleteListener(AccountActivity.getInstance(), new OnCompleteListener<Void>()
                            {
                                @Override
                                public void onComplete(@NonNull Task<Void> task)
                                {
                                    if(task.isSuccessful())
                                    {
                                        LoginData.setEmail(newEmail);
                                        FirebaseAuth.getInstance().getCurrentUser().sendEmailVerification();
                                        onSuccess.run();
                                    }
                                    else
                                    {
                                        onError.run(task.getException().getMessage());
                                    }
                                }
                            });
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError)
                        {
                            onError.run(databaseError.getMessage().toString());
                        }
                    });
                }
                else
                {
                    onError.run(task.getException().getMessage());
                }
            }
        });
    }

    public static void changePassword(final String currentPassword, final String newPassword, final Runnable onSuccess, final SingleArgRunnable<String> onError)
    {
        AuthCredential credential = EmailAuthProvider.getCredential(LoginData.getEmail(), currentPassword);
        FirebaseAuth.getInstance().getCurrentUser().reauthenticate(credential).addOnCompleteListener(LoginActivity.getInstance(), new OnCompleteListener<Void>()
        {
            @Override
            public void onComplete(@NonNull Task<Void> task)
            {
                if(task.isSuccessful())
                {
                    FirebaseAuth.getInstance().getCurrentUser().updatePassword(newPassword).addOnCompleteListener(LoginActivity.getInstance(), new OnCompleteListener<Void>()
                    {
                        @Override
                        public void onComplete(@NonNull Task<Void> task)
                        {
                            if(task.isSuccessful())
                            {
                                onSuccess.run();
                            }
                            else
                            {
                                onError.run(task.getException().getMessage());
                            }
                        }
                    });
                }
                else
                {
                    onError.run(task.getException().getMessage());
                }
            }
        });
    }

}