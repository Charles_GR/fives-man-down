package com.charles.fives_man_down.view.templates;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.*;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.*;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.view.views.main.LoginActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuth.AuthStateListener;

public abstract class AppMainActivity extends AppActivity implements OnClickListener, OnLongClickListener
{

    protected TextView tvActionBarTitle;

    private static FirebaseAuth previousFirebaseAuth;
    private static boolean appInForeground;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        FirebaseAuth.getInstance().addAuthStateListener(authStateListener);
        if(getActionBar() != null && getActionBar().getCustomView() == null)
        {
            getActionBar().setDisplayShowCustomEnabled(true);
            View customView = LayoutInflater.from(this).inflate(R.layout.action_layout_lobby, null);
            customView.findViewById(R.id.ivLogout).setOnClickListener(this);
            customView.findViewById(R.id.ivLogout).setOnLongClickListener(this);
            getActionBar().setCustomView(customView);
            tvActionBarTitle = (TextView)getActionBar().getCustomView().findViewById(R.id.tvActionBarTitle);
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        appInForeground = true;
        checkLoginState();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        appInForeground = false;
    }

    @Override
    public void onClick(View v)
    {
        FirebaseAuth.getInstance().signOut();
    }

    @Override
    public boolean onLongClick(View v)
    {
        Toast.makeText(this, R.string.logout, Toast.LENGTH_SHORT).show();
        return true;
    }

    private AuthStateListener authStateListener = new AuthStateListener()
    {
        @Override
        public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth)
        {
            if(previousFirebaseAuth != null && firebaseAuth.getCurrentUser() == null && appInForeground)
            {
                Intent intent = new Intent(AppMainActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
            previousFirebaseAuth = firebaseAuth;
        }
    };

}
