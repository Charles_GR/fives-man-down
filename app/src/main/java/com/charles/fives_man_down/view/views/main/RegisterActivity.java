package com.charles.fives_man_down.view.views.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.runnable.SingleArgRunnable;
import com.charles.fives_man_down.processing.database.LoginData;
import com.charles.fives_man_down.view.templates.*;
import com.google.firebase.auth.FirebaseAuth;

public class RegisterActivity extends AppBasicNavActivity implements OnClickListener
{

    private Button btnRegister;
    private EditText etUsername;
    private EditText etEmail;
    private EditText etPassword;
    private EditText etConfirmPassword;
    private ProgressBar pbProgress;
    private TextView tvErrorMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        btnRegister = (Button)findViewById(R.id.btnRegister);
        etUsername = (EditText)findViewById(R.id.etUsername);
        etEmail = (EditText)findViewById(R.id.etEmail);
        etPassword = (EditText)findViewById(R.id.etPassword);
        etConfirmPassword = (EditText)findViewById(R.id.etConfirmPassword);
        pbProgress = (ProgressBar)findViewById(R.id.pbProgress);
        tvErrorMessage = (TextView)findViewById(R.id.tvErrorMessage);
        btnRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if(v.getId() == R.id.btnRegister)
        {
            clearMessages();
            if(validInputs())
            {
                setActionInProgress();
                LoginData.register(etUsername.getText().toString(), etEmail.getText().toString(), etPassword.getText().toString(), registerOnSuccess, registerOnError);
            }
        }
    }

    public boolean validInputs()
    {
        if(etUsername.getText().toString().isEmpty())
        {
            etUsername.requestFocus();
            etUsername.setError(getString(R.string.username_empty));
            return false;
        }
        if(etEmail.getText().toString().isEmpty())
        {
            etEmail.requestFocus();
            etEmail.setError(getString(R.string.email_empty));
            return false;
        }
        if(etPassword.getText().toString().isEmpty())
        {
            etPassword.requestFocus();
            etPassword.setError(getString(R.string.password_empty));
            return false;
        }
        if(etConfirmPassword.getText().toString().isEmpty())
        {
            etConfirmPassword.requestFocus();
            etConfirmPassword.setError(getString(R.string.confirm_password_empty));
            return false;
        }
        if(!etPassword.getText().toString().equals(etConfirmPassword.getText().toString()))
        {
            etPassword.requestFocus();
            etPassword.setError(getString(R.string.password_not_match));
            return false;
        }
        if(etPassword.getText().toString().length() < 6)
        {
            etPassword.requestFocus();
            etPassword.setError(getString(R.string.new_password_too_short));
            return false;
        }
        return true;
    }

    private void clearMessages()
    {
        etUsername.setError(null);
        etEmail.setError(null);
        etPassword.setError(null);
        etConfirmPassword.setError(null);
        tvErrorMessage.setText("");
    }

    private void setActionInProgress()
    {
        etUsername.setEnabled(false);
        etEmail.setEnabled(false);
        etPassword.setEnabled(false);
        etConfirmPassword.setEnabled(false);
        btnRegister.setEnabled(false);
        pbProgress.setVisibility(View.VISIBLE);
    }

    private void setActionComplete()
    {
        etUsername.setEnabled(true);
        etEmail.setEnabled(true);
        etPassword.setEnabled(true);
        etConfirmPassword.setEnabled(true);
        btnRegister.setEnabled(true);
        pbProgress.setVisibility(View.GONE);
    }

    private final Runnable registerOnSuccess = new Runnable()
    {
        @Override
        public void run()
        {
            FirebaseAuth.getInstance().signOut();
            Toast.makeText(RegisterActivity.this, R.string.account_registered, Toast.LENGTH_LONG).show();
            setActionComplete();
            startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
        }
    };

    private final SingleArgRunnable<String> registerOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            tvErrorMessage.setText(text);
            setActionComplete();
        }
    };

    @Override
    protected void onUpButtonPressed()
    {
        startActivity(new Intent(this, LoginActivity.class));
    }

}

