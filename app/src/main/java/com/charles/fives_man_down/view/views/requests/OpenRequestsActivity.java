package com.charles.fives_man_down.view.views.requests;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.database.Request;
import com.charles.fives_man_down.view.templates.AppActivity;

public class OpenRequestsActivity extends AppActivity implements OnClickListener
{

    private static OpenRequestsActivity instance;

    protected OpenRequestsListView openRequestsListView;
    protected ViewOpenRequestView viewOpenRequestView;
    protected ViewFlipper vfOpenRequests;
    protected Request request;

    public static OpenRequestsActivity getInstance()
    {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_open_requests);
        super.onCreate(savedInstanceState);
        instance = this;
        openRequestsListView = new OpenRequestsListView(this);
        viewOpenRequestView = new ViewOpenRequestView(this);
        vfOpenRequests = (ViewFlipper)findViewById(R.id.vfOpenRequests);
    }

    @Override
    public void onClick(View v)
    {
        RequestsActivity.getInstance().onClick(v);
    }

    public void onLocationChanged()
    {
        openRequestsListView.updateOpenRequests();
    }

    public void onUpButtonPressed()
    {
        switch(vfOpenRequests.getDisplayedChild())
        {
            case 0:
                goToLobby();
                break;
            case 1:
                vfOpenRequests.setDisplayedChild(0);
                break;
        }
    }

}
