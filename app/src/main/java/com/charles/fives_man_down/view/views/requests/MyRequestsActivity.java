package com.charles.fives_man_down.view.views.requests;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.database.*;
import com.charles.fives_man_down.view.templates.AppActivity;

public class MyRequestsActivity extends AppActivity implements OnClickListener
{

    private static MyRequestsActivity instance;

    protected ViewFlipper vfMyRequests;
    protected MyRequestsListView myRequestsListView;
    protected CreateRequestView createRequestView;
    protected ViewMyRequestView viewMyRequestView;
    protected Request request;

    public static MyRequestsActivity getInstance()
    {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_my_requests);
        super.onCreate(savedInstanceState);
        instance = this;
        vfMyRequests = (ViewFlipper)findViewById(R.id.vfMyRequests);
        myRequestsListView = new MyRequestsListView(this);
        createRequestView = new CreateRequestView(this);
        viewMyRequestView = new ViewMyRequestView(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        createRequestView.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v)
    {
        RequestsActivity.getInstance().onClick(v);
    }

    public void onLocationChanged()
    {
        myRequestsListView.updateMyRequests();
    }

    protected void onUpButtonPressed()
    {
        switch(vfMyRequests.getDisplayedChild())
        {
            case 0:
                goToLobby();
                break;
            case 1:
                vfMyRequests.setDisplayedChild(0);
                break;
            case 2:
                vfMyRequests.setDisplayedChild(0);
                break;
        }
    }

}
