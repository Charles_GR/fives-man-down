package com.charles.fives_man_down.view.views.profiles;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.adapters.FeedbackAdapter;
import com.charles.fives_man_down.model.adapters.FilterAdapter;
import com.charles.fives_man_down.model.database.Feedback;
import com.charles.fives_man_down.model.runnable.ListRunnable;
import com.charles.fives_man_down.model.runnable.SingleArgRunnable;
import java.util.List;

public class ProfileFeedbackListView implements OnClickListener, OnItemSelectedListener, TextWatcher
{

    private static ProfileFeedbackListView instance;

    private OtherProfilesActivity parent;
    private Button btnCreateFeedback;
    private Button btnViewFeedback;
    private EditText etFeedbackSearchQuery;
    private ListView lvFeedback;
    private ProgressBar pbFeedbackList;
    private Spinner spnFeedbackSortField;
    private Spinner spnFeedbackSortOrdering;
    private Spinner spnFeedbackSearchField;
    protected TextView tvFeedback;
    private TextView tvNoFeedbackYet;
    private TextView tvFeedbackListError;

    public static ProfileFeedbackListView getInstance()
    {
        return instance;
    }

    public ProfileFeedbackListView(OtherProfilesActivity parent)
    {
        this.parent = parent;
        instance = this;
        btnCreateFeedback = (Button)parent.findViewById(R.id.btnCreateFeedback);
        btnViewFeedback = (Button)parent.findViewById(R.id.btnViewFeedbackB);
        etFeedbackSearchQuery = (EditText)parent.findViewById(R.id.etFeedbackSearchQuery);
        lvFeedback = (ListView)parent.findViewById(R.id.lvFeedback);
        pbFeedbackList = (ProgressBar)parent.findViewById(R.id.pbFeedbackList);
        spnFeedbackSortField = (Spinner)parent.findViewById(R.id.spnFeedbackSortField);
        spnFeedbackSortOrdering = (Spinner)parent.findViewById(R.id.spnFeedbackSortOrdering);
        spnFeedbackSortField = (Spinner)parent.findViewById(R.id.spnFeedbackSortField);
        spnFeedbackSearchField = (Spinner)parent.findViewById(R.id.spnFeedbackSearchField);
        tvFeedback = (TextView)parent.findViewById(R.id.tvFeedback);
        tvNoFeedbackYet = (TextView)parent.findViewById(R.id.tvNoFeedbackYet);
        tvFeedbackListError = (TextView)parent.findViewById(R.id.tvFeedbackListError);
        btnCreateFeedback.setOnClickListener(this);
        btnViewFeedback.setOnClickListener(this);
        etFeedbackSearchQuery.addTextChangedListener(this);
        spnFeedbackSortField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, FeedbackAdapter.sortFields));
        spnFeedbackSortField.setOnItemSelectedListener(this);
        spnFeedbackSortOrdering.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, FilterAdapter.sortOrderings));
        spnFeedbackSortOrdering.setOnItemSelectedListener(this);
        spnFeedbackSearchField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, FeedbackAdapter.searchFields));
        spnFeedbackSearchField.setOnItemSelectedListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnCreateFeedback:
                parent.createProfileFeedbackView.clearForm();
                parent.vfOtherProfiles.setDisplayedChild(3);
                break;
            case R.id.btnViewFeedbackB:
                viewFeedback();
                break;
            default:
                parent.onClick(v);
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        searchAndSortFeedback();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {
        searchAndSortFeedback();
    }

    @Override
    public void afterTextChanged(Editable s)
    {

    }

    private void searchAndSortFeedback()
    {
        lvFeedback.clearChoices();
        FeedbackAdapter feedbackAdapter = (FeedbackAdapter)lvFeedback.getAdapter();
        if(feedbackAdapter != null)
        {
            feedbackAdapter.searchItems(spnFeedbackSearchField.getSelectedItem().toString(), etFeedbackSearchQuery.getText().toString());
            feedbackAdapter.sortItems(spnFeedbackSortField.getSelectedItem().toString(), spnFeedbackSortOrdering.getSelectedItem().toString());
        }
    }

    private void viewFeedback()
    {
        parent.selectedFeedback = (Feedback)lvFeedback.getItemAtPosition(lvFeedback.getCheckedItemPosition());
        if(parent.selectedFeedback == null)
        {
            tvFeedback.requestFocus();
            tvFeedback.setError(parent.getString(R.string.no_feedback_selected));
        }
        else
        {
            parent.vfOtherProfiles.setDisplayedChild(4);
            parent.viewProfileFeedbackView.viewFeedback();
        }
    }

    protected final ListRunnable<Feedback> getFeedbackOnSuccess = new ListRunnable<Feedback>()
    {
        @Override
        public void run(List<Feedback> comments)
        {
            lvFeedback.setAdapter(new FeedbackAdapter(parent, comments));
            tvNoFeedbackYet.setVisibility(comments.isEmpty() ? View.VISIBLE : View.GONE);
            pbFeedbackList.setVisibility(View.GONE);
            tvFeedbackListError.setVisibility(View.GONE);
            btnViewFeedback.setEnabled(true);
            searchAndSortFeedback();
        }
    };

    protected final SingleArgRunnable<String> getFeedbackOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            lvFeedback.setAdapter(new FeedbackAdapter(parent));
            tvNoFeedbackYet.setVisibility(View.GONE);
            pbFeedbackList.setVisibility(View.GONE);
            tvFeedbackListError.setText(text);
            tvFeedbackListError.setVisibility(View.VISIBLE);
            btnViewFeedback.setEnabled(true);
        }
    };

}