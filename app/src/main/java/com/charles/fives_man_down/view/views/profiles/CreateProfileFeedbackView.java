package com.charles.fives_man_down.view.views.profiles;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.database.Feedback;
import com.charles.fives_man_down.model.runnable.SingleArgRunnable;
import com.charles.fives_man_down.processing.database.ProfilesData;

public class CreateProfileFeedbackView implements OnClickListener
{

    private OtherProfilesActivity parent;
    private Button btnSave;
    private Button btnCancel;
    private EditText etText;
    private ProgressBar pbProgress;
    private Spinner spnRating;
    private Spinner spnPerceivedExperienceLevel;

    public CreateProfileFeedbackView(OtherProfilesActivity parent)
    {
        this.parent = parent;
        btnSave = (Button)parent.findViewById(R.id.btnSave);
        btnCancel = (Button)parent.findViewById(R.id.btnCancel);
        etText = (EditText)parent.findViewById(R.id.etTextA);
        pbProgress = (ProgressBar)parent.findViewById(R.id.pbProgress);
        spnRating = (Spinner)parent.findViewById(R.id.spnRating);
        btnSave.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        spnPerceivedExperienceLevel = (Spinner)parent.findViewById(R.id.spnPerceivedExperienceLevel);
        spnRating.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, Feedback.ratings));
        spnPerceivedExperienceLevel.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, Feedback.experienceLevels));
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnSave:
                saveFeedback();
                break;
            case R.id.btnCancel:
                parent.vfOtherProfiles.setDisplayedChild(1);
                break;
            default:
                parent.onClick(v);
                break;
        }
    }

    private void saveFeedback()
    {
        etText.setError(null);
        if(validInputs())
        {
            setActionInProgress();
            ProfilesData.saveProfileFeedback(parent.selectedProfile.getUsername(), spnRating.getSelectedItem().toString(), spnPerceivedExperienceLevel.getSelectedItem().toString(),
                    etText.getText().toString(), saveFeedbackOnSuccess, saveFeedbackOnError);
            parent.vfOtherProfiles.setDisplayedChild(2);
        }
    }

    protected void clearForm()
    {
        etText.setError(null);
        spnRating.setSelection(0);
        spnPerceivedExperienceLevel.setSelection(0);
        etText.setText("");
    }

    private boolean validInputs()
    {
        if(etText.getText().toString().isEmpty())
        {
            etText.requestFocus();
            etText.setError(parent.getString(R.string.text_empty));
            return false;
        }
        return true;
    }

    private void setActionInProgress()
    {
        spnRating.setEnabled(false);
        spnPerceivedExperienceLevel.setEnabled(false);
        etText.setEnabled(false);
        btnSave.setEnabled(false);
        btnCancel.setEnabled(false);
        pbProgress.setVisibility(View.VISIBLE);
    }

    private void setActionComplete()
    {
        spnRating.setEnabled(true);
        spnPerceivedExperienceLevel.setEnabled(true);
        etText.setEnabled(true);
        btnSave.setEnabled(true);
        btnCancel.setEnabled(true);
        pbProgress.setVisibility(View.GONE);
    }

    private final Runnable saveFeedbackOnSuccess = new Runnable()
    {
        @Override
        public void run()
        {
            Toast.makeText(parent, R.string.feedback_successfully_saved, Toast.LENGTH_SHORT).show();
            setActionComplete();
        }
    };

    private final SingleArgRunnable<String> saveFeedbackOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            Toast.makeText(parent, text, Toast.LENGTH_SHORT).show();
            setActionComplete();
        }
    };

}
