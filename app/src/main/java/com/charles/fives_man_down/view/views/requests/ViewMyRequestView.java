package com.charles.fives_man_down.view.views.requests;

import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.database.Request;
import com.charles.fives_man_down.model.runnable.SingleArgRunnable;
import com.charles.fives_man_down.processing.database.RequestsData;
import java.text.SimpleDateFormat;

public class ViewMyRequestView implements OnClickListener
{

    private MyRequestsActivity parent;
    private Button btnViewLocation;
    private Button btnDelete;
    private EditText etDescription;
    private EditText etPlayersNeeded;
    private EditText etLocation;
    private EditText etStartTime;
    private EditText etAcceptees;

    public ViewMyRequestView(MyRequestsActivity parent)
    {
        this.parent = parent;
        btnViewLocation = (Button)parent.findViewById(R.id.btnViewLocation);
        btnDelete = (Button)parent.findViewById(R.id.btnDelete);
        etDescription = (EditText)parent.findViewById(R.id.etDescriptionB);
        etPlayersNeeded = (EditText)parent.findViewById(R.id.etPlayersNeededB);
        etLocation = (EditText)parent.findViewById(R.id.etLocationB);
        etStartTime = (EditText)parent.findViewById(R.id.etStartTimeB);
        etAcceptees = (EditText)parent.findViewById(R.id.etAcceptees);
        btnViewLocation.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnViewLocation:
                Intent mapsIntent = new Intent(Intent.ACTION_VIEW);
                mapsIntent.setData(Uri.parse("geo:" + parent.request.getPosition().getLatitude() + "," + parent.request.getPosition().getLongitude() + "?q=" +
                        parent.request.getPosition().getLatitude() + "," + parent.request.getPosition().getLongitude()));
                parent.startActivity(mapsIntent);
                break;
            case R.id.btnDelete:
                RequestsData.deleteRequest(parent.request.getKey(), deleteRequestOnSuccess, deleteRequestOnError);
                parent.vfMyRequests.setDisplayedChild(0);
                break;
            default:
                parent.onClick(v);
                break;
        }
    }

    protected void viewSelectedRequest()
    {
        etDescription.setText(parent.request.getDescription());
        etPlayersNeeded.setText(String.valueOf(parent.request.getPlayersNeeded()));
        etLocation.setText(parent.request.getPosition().toString());
        etStartTime.setText(SimpleDateFormat.getInstance().format(parent.request.getStartTime()));
        etAcceptees.setText(TextUtils.join(", ", parent.request.getAcceptees()));
    }

    protected void viewRequest(Request request)
    {
        parent.request = request;
        viewSelectedRequest();
    }

    private final Runnable deleteRequestOnSuccess = new Runnable()
    {
        @Override
        public void run()
        {
            Toast.makeText(ViewMyRequestView.this.parent, R.string.request_successfully_deleted, Toast.LENGTH_SHORT).show();
            parent.vfMyRequests.setDisplayedChild(0);
        }
    };

    private final SingleArgRunnable<String> deleteRequestOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            Toast.makeText(ViewMyRequestView.this.parent, text, Toast.LENGTH_SHORT).show();
        }
    };

}

