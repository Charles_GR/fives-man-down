package com.charles.fives_man_down.view.views.profiles;

import android.content.Intent;
import android.os.Bundle;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.view.templates.TabbedActivity;

public class ProfilesActivity extends TabbedActivity
{

    private static ProfilesActivity instance;

    public static ProfilesActivity getInstance()
    {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_profiles);
        super.onCreate(savedInstanceState);
        instance = this;
        tvActionBarTitle.setText(R.string.profiles_title);
        createTab(getString(R.string.my_profile_title), MyProfileActivity.class);
        createTab(getString(R.string.other_profiles_title), OtherProfilesActivity.class);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        MyProfileActivity.getInstance().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onUpButtonPressed()
    {
        switch(thTabbedView.getCurrentTab())
        {
            case 0:
                MyProfileActivity.getInstance().onUpButtonPressed();
                break;
            case 1:
                OtherProfilesActivity.getInstance().onUpButtonPressed();
                break;
        }
    }

}
