package com.charles.fives_man_down.view.views.requests;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.Toast;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.MyApplication;
import com.charles.fives_man_down.model.database.Notification;
import com.charles.fives_man_down.model.database.Request;
import com.charles.fives_man_down.processing.database.LoginData;
import com.charles.fives_man_down.view.templates.TabbedLocationActivity;
import com.google.gson.Gson;

public class RequestsActivity extends TabbedLocationActivity
{

    private static RequestsActivity instance;

    public static RequestsActivity getInstance()
    {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_requests);
        super.onCreate(savedInstanceState);
        instance = this;
        tvActionBarTitle.setText(R.string.requests_title);
        createTab(getString(R.string.my_requests_title), MyRequestsActivity.class);
        createTab(getString(R.string.open_requests_title), OpenRequestsActivity.class);
        createTab(getString(R.string.accepted_requests_title), AcceptedRequestsActivity.class);
        if(getIntent().getExtras() != null && getIntent().getExtras().getBoolean("FromNotification"))
        {
            displayRequestFromNotification();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        MyRequestsActivity.getInstance().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
        {
            initialiseLocation();
            notifyLocationChanged();
        }
        else
        {
            finish();
        }
    }

    @Override
    public void onLocationChanged(Location location)
    {
        MyApplication.getInstance().setUserPosition(location);
        notifyLocationChanged();
    }

    private void notifyLocationChanged()
    {
        switch(thTabbedView.getCurrentTab())
        {
            case 0:
                MyRequestsActivity.getInstance().onLocationChanged();
                break;
            case 1:
                OpenRequestsActivity.getInstance().onLocationChanged();
                break;
            case 2:
                AcceptedRequestsActivity.getInstance().onLocationChanged();
                break;
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras)
    {

    }

    @Override
    public void onProviderEnabled(String provider)
    {

    }

    @Override
    public void onProviderDisabled(String provider)
    {
        Toast.makeText(this, R.string.enable_location_services, Toast.LENGTH_SHORT).show();
        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        finish();
    }

    @Override
    protected void onUpButtonPressed()
    {
        switch(thTabbedView.getCurrentTab())
        {
            case 0:
                MyRequestsActivity.getInstance().onUpButtonPressed();
                break;
            case 1:
                OpenRequestsActivity.getInstance().onUpButtonPressed();
                break;
            case 2:
                AcceptedRequestsActivity.getInstance().onUpButtonPressed();
                break;
        }
    }

    private void displayRequestFromNotification()
    {
        Request request = new Gson().fromJson(getIntent().getStringExtra("Request"), Request.class);
        switch(getIntent().getStringExtra("NotificationTitle"))
        {
            case Notification.NEW_PLAYER_REQUEST:
                if(request.getAcceptees().contains(LoginData.getUsername()))
                {
                    thTabbedView.setCurrentTab(2);
                    AcceptedRequestsActivity.getInstance().vfAcceptedRequests.setDisplayedChild(1);
                    AcceptedRequestsActivity.getInstance().viewAcceptedRequestView.viewRequest(request);
                }
                else
                {
                    thTabbedView.setCurrentTab(1);
                    OpenRequestsActivity.getInstance().vfOpenRequests.setDisplayedChild(1);
                    OpenRequestsActivity.getInstance().viewOpenRequestView.viewRequest(request);
                }
                break;
            case Notification.PLAYER_REQUEST_ACCEPTED:
            case Notification.PLAYER_REQUEST_UNACCEPTED:
                thTabbedView.setCurrentTab(0);
                MyRequestsActivity.getInstance().vfMyRequests.setDisplayedChild(2);
                MyRequestsActivity.getInstance().viewMyRequestView.viewRequest(request);
                break;
        }
    }

}
