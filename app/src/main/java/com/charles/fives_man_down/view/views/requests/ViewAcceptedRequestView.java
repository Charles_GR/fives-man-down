package com.charles.fives_man_down.view.views.requests;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.database.Request;
import com.charles.fives_man_down.model.runnable.SingleArgRunnable;
import com.charles.fives_man_down.processing.database.RequestsData;
import java.text.SimpleDateFormat;

public class ViewAcceptedRequestView implements OnClickListener
{

    private AcceptedRequestsActivity parent;
    private Button btnViewLocation;
    private Button btnUnaccept;
    private Button btnCancel;
    private EditText etDescription;
    private EditText etPlayersNeeded;
    private EditText etLocation;
    private EditText etStartTime;

    public ViewAcceptedRequestView(AcceptedRequestsActivity parent)
    {
        this.parent = parent;
        btnViewLocation = (Button)parent.findViewById(R.id.btnViewLocation);
        btnUnaccept = (Button)parent.findViewById(R.id.btnUnaccept);
        btnCancel = (Button)parent.findViewById(R.id.btnCancel);
        etDescription = (EditText)parent.findViewById(R.id.etDescription);
        etPlayersNeeded = (EditText)parent.findViewById(R.id.etPlayersNeeded);
        etLocation = (EditText)parent.findViewById(R.id.etLocation);
        etStartTime = (EditText)parent.findViewById(R.id.etStartTime);
        btnViewLocation.setOnClickListener(this);
        btnUnaccept.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnViewLocation:
                Intent mapsIntent = new Intent(Intent.ACTION_VIEW);
                mapsIntent.setData(Uri.parse("geo:" + parent.request.getPosition().getLatitude() + "," + parent.request.getPosition().getLongitude() + "?q=" +
                        parent.request.getPosition().getLatitude() + "," + parent.request.getPosition().getLongitude()));
                parent.startActivity(mapsIntent);
                break;
            case R.id.btnUnaccept:
                btnViewLocation.setEnabled(false);
                btnUnaccept.setEnabled(false);
                btnCancel.setEnabled(false);
                RequestsData.unacceptRequest(parent.request.getKey(), unacceptRequestOnSuccess, unacceptRequestOnError);
                break;
            case R.id.btnCancel:
                parent.vfAcceptedRequests.setDisplayedChild(0);
                break;
            default:
                parent.onClick(v);
                break;
        }
    }

    protected void viewSelectedRequest()
    {
        etDescription.setText(parent.request.getDescription());
        etPlayersNeeded.setText(String.valueOf(parent.request.getPlayersNeeded()));
        etLocation.setText(parent.request.getPosition().toString());
        etStartTime.setText(SimpleDateFormat.getInstance().format(parent.request.getStartTime()));
    }

    protected void viewRequest(Request request)
    {
        parent.request = request;
        viewSelectedRequest();
    }

    private final Runnable unacceptRequestOnSuccess = new Runnable()
    {
        @Override
        public void run()
        {
            Toast.makeText(ViewAcceptedRequestView.this.parent, R.string.request_unaccepted, Toast.LENGTH_SHORT).show();
            parent.vfAcceptedRequests.setDisplayedChild(0);
            btnViewLocation.setEnabled(true);
            btnUnaccept.setEnabled(true);
            btnCancel.setEnabled(true);
        }
    };

    private final SingleArgRunnable<String> unacceptRequestOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            Toast.makeText(ViewAcceptedRequestView.this.parent, text, Toast.LENGTH_SHORT).show();
            btnViewLocation.setEnabled(true);
            btnUnaccept.setEnabled(true);
            btnCancel.setEnabled(true);
        }
    };

}

