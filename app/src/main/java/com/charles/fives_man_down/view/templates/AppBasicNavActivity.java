package com.charles.fives_man_down.view.templates;

import android.os.Bundle;
import android.view.*;

public abstract class AppBasicNavActivity extends AppActivity
{

        @Override
        protected void onCreate(Bundle savedInstanceState)
        {
            if(getActionBar() != null)
            {
                getActionBar().setDisplayHomeAsUpEnabled(true);
            }
            super.onCreate(savedInstanceState);
            getActionBar().setCustomView(null);
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item)
        {
            if(item.getItemId() == android.R.id.home)
            {
                onUpButtonPressed();
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        protected abstract void onUpButtonPressed();

}
