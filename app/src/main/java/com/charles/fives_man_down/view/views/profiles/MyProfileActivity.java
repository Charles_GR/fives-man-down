package com.charles.fives_man_down.view.views.profiles;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.database.Profile;
import com.charles.fives_man_down.model.runnable.SingleArgRunnable;
import com.charles.fives_man_down.processing.database.ProfilesData;
import com.charles.fives_man_down.processing.media.ImageParser;
import com.charles.fives_man_down.view.templates.AppActivity;
import java.io.File;

public class MyProfileActivity extends AppActivity implements OnClickListener
{

    private static MyProfileActivity instance;
    private static final int IMAGE_SIZE_LIMIT = 314572800;

    private Button btnSelectPhoto;
    private Button btnRemovePhoto;
    private Button btnSave;
    private Button btnReload;
    private EditText etFullName;
    private EditText etBio;
    private ImageView ivPhoto;
    private ProgressBar pbProgress;
    private Spinner spnExperienceLevel;
    private TextView tvExperienceLevel;

    public static MyProfileActivity getInstance()
    {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        instance = this;
        setContentView(R.layout.activity_my_profile);
        btnSelectPhoto = (Button)findViewById(R.id.btnSelectPhoto);
        btnRemovePhoto = (Button)findViewById(R.id.btnRemovePhoto);
        btnSave = (Button)findViewById(R.id.btnSave);
        btnReload = (Button)findViewById(R.id.btnReload);
        etFullName = (EditText)findViewById(R.id.etFullName);
        etBio = (EditText)findViewById(R.id.etBio);
        ivPhoto = (ImageView)findViewById(R.id.ivPhoto);
        pbProgress = (ProgressBar)findViewById(R.id.pbProgress);
        spnExperienceLevel = (Spinner)findViewById(R.id.spnExperienceLevel);
        tvExperienceLevel = (TextView)findViewById(R.id.tvExperienceLevel);
        btnSelectPhoto.setOnClickListener(this);
        btnRemovePhoto.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        btnReload.setOnClickListener(this);
        spnExperienceLevel.setAdapter(new ArrayAdapter<>(this, R.layout.spinner_item, new String[]{"Beginner", "Amateur", "Intermediate", "Expert", "Professional"}));
        setActionInProgress();
        ProfilesData.loadMyProfile(false, loadProfileOnSuccess, loadProfileOnError);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnSelectPhoto:
                ProfilesActivity.getInstance().startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), 0);
                break;
            case R.id.btnRemovePhoto:
                ivPhoto.setImageDrawable(null);
                ivPhoto.setTag("");
                break;
            case R.id.btnSave:
                saveProfile();
                break;
            case R.id.btnReload:
                setActionInProgress();
                ProfilesData.loadMyProfile(true, reloadProfileOnSuccess, loadProfileOnError);
                break;
            default:
                ProfilesActivity.getInstance().onClick(v);
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode != Activity.RESULT_OK || data == null)
        {
            return;
        }
        try
        {
            Cursor cursor = getContentResolver().query(data.getData(), new String[]{MediaStore.Images.Media.DATA}, null, null, null);
            cursor.moveToFirst();
            String imagePath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            cursor.close();
            if(new File(imagePath).length() > IMAGE_SIZE_LIMIT)
            {
                Toast.makeText(this, R.string.selected_image_too_large, Toast.LENGTH_SHORT).show();
                return;
            }
            ivPhoto.setImageBitmap(BitmapFactory.decodeFile(imagePath));
            ivPhoto.setTag(imagePath);
        }
        catch(Exception e)
        {
            Toast.makeText(this, R.string.failed_to_load_image, Toast.LENGTH_SHORT).show();
        }
    }

    protected void onUpButtonPressed()
    {
        goToLobby();
    }

    private void saveProfile()
    {
        clearErrors();
        if(!validInputs())
        {
            return;
        }
        setActionInProgress();
        ProfilesData.saveMyProfile(etFullName.getText().toString(), etBio.getText().toString(), ImageParser.encodeImage(ivPhoto),
                spnExperienceLevel.getSelectedItem().toString(), saveProfileOnSuccess, saveProfileOnError);
    }

    private void clearErrors()
    {
        etFullName.setError(null);
        etBio.setError(null);
        tvExperienceLevel.setError(null);
    }

    private boolean validInputs()
    {
        if(etFullName.getText().toString().isEmpty())
        {
            etFullName.requestFocus();
            etFullName.setError(getString(R.string.full_name_empty));
            return false;
        }
        if(etBio.getText().toString().isEmpty())
        {
            etBio.requestFocus();
            etBio.setError(getString(R.string.bio_empty));
            return false;
        }
        if(spnExperienceLevel.getSelectedItem().toString().equals(""))
        {
            tvExperienceLevel.requestFocus();
            tvExperienceLevel.setError(getString(R.string.experience_level_not_set));
            return false;
        }
        return true;
    }

    private void setActionInProgress()
    {
        btnSelectPhoto.setEnabled(false);
        btnRemovePhoto.setEnabled(false);
        btnSave.setEnabled(false);
        btnReload.setEnabled(false);
        etFullName.setEnabled(false);
        etBio.setEnabled(false);
        spnExperienceLevel.setEnabled(false);
        pbProgress.setVisibility(View.VISIBLE);
    }

    private void setActionComplete()
    {
        btnSelectPhoto.setEnabled(true);
        btnRemovePhoto.setEnabled(true);
        btnSave.setEnabled(true);
        btnReload.setEnabled(true);
        etFullName.setEnabled(true);
        etBio.setEnabled(true);
        spnExperienceLevel.setEnabled(true);
        pbProgress.setVisibility(View.GONE);
    }

    private final Runnable saveProfileOnSuccess = new Runnable()
    {
        @Override
        public void run()
        {
            Toast.makeText(MyProfileActivity.this, R.string.profile_successfully_saved, Toast.LENGTH_SHORT).show();
            setActionComplete();
        }
    };

    private final SingleArgRunnable<String> saveProfileOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            Toast.makeText(MyProfileActivity.this, text, Toast.LENGTH_SHORT).show();
            setActionComplete();
        }
    };

    private final SingleArgRunnable<Profile> reloadProfileOnSuccess = new SingleArgRunnable<Profile>()
    {
        @Override
        public void run(Profile profile)
        {
            loadProfileOnSuccess.run(profile);
            Toast.makeText(MyProfileActivity.this, R.string.profile_successfully_reloaded, Toast.LENGTH_SHORT).show();
            setActionComplete();
        }
    };

    private final SingleArgRunnable<Profile> loadProfileOnSuccess = new SingleArgRunnable<Profile>()
    {
        @Override
        public void run(Profile profile)
        {
            if(profile != null)
            {
                etFullName.setText(profile.getFullName());
                etBio.setText(profile.getBio());
                ivPhoto.setImageBitmap(profile.getPhoto() == null ? null : ImageParser.decodeImage(profile.getPhoto()));
                spnExperienceLevel.setSelection(((ArrayAdapter<String>) spnExperienceLevel.getAdapter()).getPosition(profile.getExperienceLevel()));
            }
            setActionComplete();
        }
    };

    private final SingleArgRunnable<String> loadProfileOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            Toast.makeText(MyProfileActivity.this, text, Toast.LENGTH_SHORT).show();
            setActionComplete();
        }
    };

}