package com.charles.fives_man_down.view.views.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.MyApplication;
import com.charles.fives_man_down.model.runnable.SingleArgRunnable;
import com.charles.fives_man_down.processing.NotificationService;
import com.charles.fives_man_down.processing.database.LoginData;
import com.charles.fives_man_down.view.templates.AppActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppActivity implements OnClickListener
{

    private static LoginActivity instance;
    private Button btnLogin;
    private Button btnRegister;
    private Button btnResetPassword;
    private EditText etEmail;
    private EditText etPassword;
    private ProgressBar pbProgress;
    private TextView tvSuccessMessage;
    private TextView tvErrorMessage;

    public static LoginActivity getInstance()
    {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        instance = this;
        btnLogin = (Button)findViewById(R.id.btnLogin);
        btnRegister = (Button)findViewById(R.id.btnRegister);
        btnResetPassword = (Button)findViewById(R.id.btnResetPassword);
        etEmail = (EditText)findViewById(R.id.etEmail);
        etPassword = (EditText)findViewById(R.id.etPassword);
        pbProgress = (ProgressBar)findViewById(R.id.pbProgress);
        tvSuccessMessage = (TextView)findViewById(R.id.tvSuccessMessage);
        tvErrorMessage = (TextView)findViewById(R.id.tvErrorMessage);
        btnLogin.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        btnResetPassword.setOnClickListener(this);
        getActionBar().setTitle(R.string.login_title);
        checkLoginState();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        etEmail.setText("");
        etPassword.setText("");
        etEmail.requestFocus();
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnLogin:
                login();
                break;
            case R.id.btnRegister:
                startActivity(new Intent(this, RegisterActivity.class));
                break;
            case R.id.btnResetPassword:
                resetPassword();
                break;
        }
    }

    @Override
    protected void checkLoginState()
    {
        setActionInProgress();
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if(firebaseUser == null)
        {
            setActionComplete();
        }
        else
        {
            LoginData.setEmail(firebaseUser.getEmail());
            LoginData.getUsernameFromEmail(LoginData.getEmail(), checkLoginOnSuccess, checkLoginOnError);
        }
    }

    private void login()
    {
        etEmail.setError(null);
        etPassword.setError(null);
        if(!validLoginInputs())
        {
            return;
        }
        setActionInProgress();
        LoginData.login(etEmail.getText().toString(), etPassword.getText().toString(), loginOnSuccess, loginOnError);
    }

    private void resetPassword()
    {
        etEmail.setError(null);
        etPassword.setError(null);
        if(!validResetPasswordInput())
        {
            return;
        }
        setActionInProgress();
        LoginData.resetPassword(etEmail.getText().toString(), resetPasswordOnSuccess, resetPasswordOnError);
    }

    private boolean validLoginInputs()
    {
        if(etEmail.getText().toString().isEmpty())
        {
            etEmail.requestFocus();
            etEmail.setError(getString(R.string.email_empty));
            return false;
        }
        if(etPassword.getText().toString().isEmpty())
        {
            etPassword.requestFocus();
            etPassword.setError(getString(R.string.password_empty));
            return false;
        }
        return true;
    }

    private boolean validResetPasswordInput()
    {
        if(etEmail.getText().toString().isEmpty())
        {
            etEmail.requestFocus();
            etEmail.setError(getString(R.string.email_empty));
            return false;
        }
        return true;
    }

    private void setActionInProgress()
    {
        hideKeyboard();
        tvErrorMessage.setText("");
        tvSuccessMessage.setText("");
        pbProgress.setVisibility(View.VISIBLE);
        btnLogin.setEnabled(false);
        btnRegister.setEnabled(false);
        btnResetPassword.setEnabled(false);
    }

    private void setActionComplete()
    {
        pbProgress.setVisibility(View.GONE);
        btnLogin.setEnabled(true);
        btnRegister.setEnabled(true);
        btnResetPassword.setEnabled(true);
    }

    private final SingleArgRunnable<String> checkLoginOnSuccess = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            LoginData.setUsername(text);
            setActionComplete();
            goToLobby();
            MyApplication.getInstance().fetchOptions();
            startService(new Intent(LoginActivity.this, NotificationService.class));
        }
    };

    private final SingleArgRunnable<String> checkLoginOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            FirebaseAuth.getInstance().signOut();
            tvErrorMessage.setText(text);
            setActionComplete();
        }
    };

    private final SingleArgRunnable<String> loginOnSuccess = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            LoginData.setEmail(text);
            LoginData.getUsernameFromEmail(LoginData.getEmail(), checkLoginOnSuccess, checkLoginOnError);
            MyApplication.getInstance().fetchOptions();
            startService(new Intent(LoginActivity.this, NotificationService.class));
        }
    };

    private final SingleArgRunnable<String> loginOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String message)
        {
            tvErrorMessage.setText(message);
            setActionComplete();
        }
    };

    private final Runnable resetPasswordOnSuccess = new Runnable()
    {
        @Override
        public void run()
        {
            tvSuccessMessage.setText(R.string.new_password_sent);
            setActionComplete();
        }
    };

    private final SingleArgRunnable<String> resetPasswordOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String message)
        {
            tvErrorMessage.setText(message);
            setActionComplete();
        }
    };

}
