package com.charles.fives_man_down.view.views.requests;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.adapters.*;
import com.charles.fives_man_down.model.database.Request;
import com.charles.fives_man_down.model.runnable.ListRunnable;
import com.charles.fives_man_down.model.runnable.SingleArgRunnable;
import com.charles.fives_man_down.processing.database.*;
import java.util.List;

public class MyRequestsListView implements OnClickListener, OnItemSelectedListener, TextWatcher
{

    private MyRequestsActivity parent;
    private Button btnCreateRequest;
    private Button btnViewRequest;
    private EditText etRequestSearchQuery;
    private ListView lvRequests;
    private Spinner spnRequestSortField;
    private Spinner spnRequestSortOrdering;
    private Spinner spnRequestSearchField;
    private ProgressBar pbRequestList;
    private TextView tvRequests;
    private TextView tvNoRequestsYet;
    private TextView tvRequestListError;

    public MyRequestsListView(MyRequestsActivity parent)
    {
        this.parent = parent;
        btnCreateRequest = (Button)parent.findViewById(R.id.btnCreateRequest);
        btnViewRequest = (Button)parent.findViewById(R.id.btnViewRequest);
        etRequestSearchQuery = (EditText)parent.findViewById(R.id.etRequestSearchQuery);
        lvRequests = (ListView)parent.findViewById(R.id.lvRequests);
        spnRequestSortField = (Spinner)parent.findViewById(R.id.spnRequestSortField);
        spnRequestSortOrdering = (Spinner)parent.findViewById(R.id.spnRequestSortOrdering);
        spnRequestSearchField = (Spinner)parent.findViewById(R.id.spnRequestSearchField);
        pbRequestList = (ProgressBar)parent.findViewById(R.id.pbRequestList);
        tvRequests = (TextView)parent.findViewById(R.id.tvRequests);
        tvNoRequestsYet = (TextView)parent.findViewById(R.id.tvNoRequestsYet);
        tvRequestListError = (TextView)parent.findViewById(R.id.tvRequestListError);
        btnCreateRequest.setOnClickListener(this);
        btnViewRequest.setOnClickListener(this);
        etRequestSearchQuery.addTextChangedListener(this);
        spnRequestSortField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, MyRequestAdapter.sortFields));
        spnRequestSortField.setOnItemSelectedListener(this);
        spnRequestSortOrdering.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, FilterAdapter.sortOrderings));
        spnRequestSortOrdering.setOnItemSelectedListener(this);
        spnRequestSearchField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, MyRequestAdapter.searchFields));
        spnRequestSearchField.setOnItemSelectedListener(this);
        RequestsActivity.getInstance().checkLocationEnabled(onLocationEnabled);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnCreateRequest:
                parent.createRequestView.clearForm();
                parent.vfMyRequests.setDisplayedChild(1);
                break;
            case R.id.btnViewRequest:
                viewRequest();
                break;
            default:
                parent.onClick(v);
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        searchAndSortRequests();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {
        searchAndSortRequests();
    }

    @Override
    public void afterTextChanged(Editable s)
    {

    }

    private void viewRequest()
    {
        parent.request = (Request)lvRequests.getItemAtPosition(lvRequests.getCheckedItemPosition());
        if(parent.request == null)
        {
            tvRequests.requestFocus();
            tvRequests.setError(parent.getString(R.string.no_request_selected));
        }
        else
        {
            parent.vfMyRequests.setDisplayedChild(2);
            parent.viewMyRequestView.viewSelectedRequest();
        }
    }

    private void searchAndSortRequests()
    {
        lvRequests.clearChoices();
        MyRequestAdapter myRequestAdapter = (MyRequestAdapter)lvRequests.getAdapter();
        if(myRequestAdapter != null)
        {
            myRequestAdapter.searchItems(spnRequestSearchField.getSelectedItem().toString(), etRequestSearchQuery.getText().toString());
            myRequestAdapter.sortItems(spnRequestSortField.getSelectedItem().toString(), spnRequestSortOrdering.getSelectedItem().toString());
        }
    }

    protected void updateMyRequests()
    {
        RequestsData.getMyRequests(getMyRequestsOnSuccess, getMyRequestsOnError);
    }

    private final Runnable onLocationEnabled = new Runnable()
    {
        @Override
        public void run()
        {
            updateMyRequests();
        }
    };

    private final ListRunnable<Request> getMyRequestsOnSuccess = new ListRunnable<Request>()
    {
        @Override
        public void run(List<Request> requests)
        {
            lvRequests.setAdapter(new MyRequestAdapter(parent, requests));
            tvNoRequestsYet.setVisibility(requests.isEmpty() ? View.VISIBLE : View.GONE);
            tvRequestListError.setVisibility(View.GONE);
            pbRequestList.setVisibility(View.GONE);
            btnCreateRequest.setEnabled(true);
            btnViewRequest.setEnabled(true);
            searchAndSortRequests();
        }
    };

    private final SingleArgRunnable<String> getMyRequestsOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            lvRequests.setAdapter(new MyRequestAdapter(parent));
            tvNoRequestsYet.setVisibility(View.GONE);
            tvRequestListError.setText(text);
            tvRequestListError.setVisibility(View.VISIBLE);
            pbRequestList.setVisibility(View.GONE);
            btnCreateRequest.setEnabled(true);
            btnViewRequest.setEnabled(true);
        }
    };

}
