package com.charles.fives_man_down.view.views.profiles;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ViewFlipper;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.database.Feedback;
import com.charles.fives_man_down.model.database.Profile;
import com.charles.fives_man_down.view.templates.AppActivity;

public class OtherProfilesActivity extends AppActivity implements OnClickListener
{

    private static OtherProfilesActivity instance;

    protected OtherProfilesListView otherProfilesListView;
    protected ViewOtherProfileView viewOtherProfileView;
    protected ProfileFeedbackListView profileFeedbackListView;
    protected CreateProfileFeedbackView createProfileFeedbackView;
    protected ViewProfileFeedbackView viewProfileFeedbackView;
    protected ViewFlipper vfOtherProfiles;
    protected Profile selectedProfile;
    protected Feedback selectedFeedback;

    public static OtherProfilesActivity getInstance()
    {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        instance = this;
        setContentView(R.layout.activity_other_profiles);
        vfOtherProfiles = (ViewFlipper)findViewById(R.id.vfOtherProfiles);
        otherProfilesListView = new OtherProfilesListView(this);
        viewOtherProfileView = new ViewOtherProfileView(this);
        profileFeedbackListView = new ProfileFeedbackListView(this);
        createProfileFeedbackView = new CreateProfileFeedbackView(this);
        viewProfileFeedbackView = new ViewProfileFeedbackView(this);
    }

    @Override
    public void onClick(View v)
    {
        ProfilesActivity.getInstance().onClick(v);
    }

    public void onUpButtonPressed()
    {
        switch(vfOtherProfiles.getDisplayedChild())
        {
            case 0:
                goToLobby();
                break;
            case 1:
                vfOtherProfiles.setDisplayedChild(0);
                break;
            case 2:
                vfOtherProfiles.setDisplayedChild(1);
                break;
            case 3:
                vfOtherProfiles.setDisplayedChild(2);
                break;
            case 4:
                vfOtherProfiles.setDisplayedChild(2);
                break;
        }
    }

}
