package com.charles.fives_man_down.view.views.requests;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.database.Request;
import com.charles.fives_man_down.model.runnable.SingleArgRunnable;
import com.charles.fives_man_down.processing.database.RequestsData;
import java.text.SimpleDateFormat;

public class ViewOpenRequestView implements OnClickListener
{

    private OpenRequestsActivity parent;
    private Button btnViewLocation;
    private Button btnAccept;
    private Button btnCancel;
    private EditText etDescription;
    private EditText etPlayersNeeded;
    private EditText etLocation;
    private EditText etStartTime;

    public ViewOpenRequestView(OpenRequestsActivity parent)
    {
        this.parent = parent;
        btnViewLocation = (Button)parent.findViewById(R.id.btnViewLocation);
        btnAccept = (Button)parent.findViewById(R.id.btnAccept);
        btnCancel = (Button)parent.findViewById(R.id.btnCancel);
        etDescription = (EditText)parent.findViewById(R.id.etDescription);
        etPlayersNeeded = (EditText)parent.findViewById(R.id.etPlayersNeeded);
        etLocation = (EditText)parent.findViewById(R.id.etLocation);
        etStartTime = (EditText)parent.findViewById(R.id.etStartTime);
        btnViewLocation.setOnClickListener(this);
        btnAccept.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnViewLocation:
                Intent mapsIntent = new Intent(Intent.ACTION_VIEW);
                mapsIntent.setData(Uri.parse("geo:" + parent.request.getPosition().getLatitude() + "," + parent.request.getPosition().getLongitude() + "?q=" +
                        parent.request.getPosition().getLatitude() + "," + parent.request.getPosition().getLongitude()));
                parent.startActivity(mapsIntent);
                break;
            case R.id.btnAccept:
                btnViewLocation.setEnabled(false);
                btnAccept.setEnabled(false);
                btnCancel.setEnabled(false);
                RequestsData.acceptRequest(parent.request.getKey(), acceptRequestOnSuccess, acceptRequestOnError);
                break;
            case R.id.btnCancel:
                parent.vfOpenRequests.setDisplayedChild(0);
                break;
            default:
                parent.onClick(v);
                break;
        }
    }

    protected void viewSelectedRequest()
    {
        etDescription.setText(parent.request.getDescription());
        etPlayersNeeded.setText(String.valueOf(parent.request.getPlayersNeeded()));
        etLocation.setText(parent.request.getPosition().toString());
        etStartTime.setText(SimpleDateFormat.getInstance().format(parent.request.getStartTime()));
    }

    protected void viewRequest(Request request)
    {
        parent.request = request;
        viewSelectedRequest();
    }

    private final Runnable acceptRequestOnSuccess = new Runnable()
    {
        @Override
        public void run()
        {
            Toast.makeText(ViewOpenRequestView.this.parent, R.string.request_accepted, Toast.LENGTH_SHORT).show();
            parent.vfOpenRequests.setDisplayedChild(0);
            btnViewLocation.setEnabled(true);
            btnAccept.setEnabled(true);
            btnCancel.setEnabled(true);
        }
    };

    private final SingleArgRunnable<String> acceptRequestOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            Toast.makeText(ViewOpenRequestView.this.parent, text, Toast.LENGTH_SHORT).show();
            btnViewLocation.setEnabled(true);
            btnAccept.setEnabled(true);
            btnCancel.setEnabled(true);
        }
    };

}
