package com.charles.fives_man_down.view.views.requests;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.adapters.FilterAdapter;
import com.charles.fives_man_down.model.adapters.OtherRequestAdapter;
import com.charles.fives_man_down.model.database.Request;
import com.charles.fives_man_down.model.runnable.ListRunnable;
import com.charles.fives_man_down.model.runnable.SingleArgRunnable;
import com.charles.fives_man_down.processing.database.RequestsData;
import java.util.List;

public class AcceptedRequestsListView implements OnClickListener, OnItemSelectedListener, TextWatcher
{

    private AcceptedRequestsActivity parent;
    private Button btnViewRequest;
    private EditText etRequestSearchQuery;
    private ListView lvRequests;
    private Spinner spnRequestSortField;
    private Spinner spnRequestSortOrdering;
    private Spinner spnRequestSearchField;
    private ProgressBar pbRequestList;
    private TextView tvRequests;
    private TextView tvNoRequestsYet;
    private TextView tvRequestListError;

    public AcceptedRequestsListView(AcceptedRequestsActivity parent)
    {
        this.parent = parent;
        btnViewRequest = (Button)parent.findViewById(R.id.btnViewRequest);
        etRequestSearchQuery = (EditText)parent.findViewById(R.id.etRequestSearchQuery);
        lvRequests = (ListView)parent.findViewById(R.id.lvRequests);
        spnRequestSortField = (Spinner)parent.findViewById(R.id.spnRequestSortField);
        spnRequestSortOrdering = (Spinner)parent.findViewById(R.id.spnRequestSortOrdering);
        spnRequestSearchField = (Spinner)parent.findViewById(R.id.spnRequestSearchField);
        pbRequestList = (ProgressBar)parent.findViewById(R.id.pbRequestList);
        tvRequests = (TextView)parent.findViewById(R.id.tvRequests);
        tvNoRequestsYet = (TextView)parent.findViewById(R.id.tvNoRequestsYet);
        tvRequestListError = (TextView)parent.findViewById(R.id.tvRequestListError);
        btnViewRequest.setOnClickListener(this);
        etRequestSearchQuery.addTextChangedListener(this);
        spnRequestSortField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, OtherRequestAdapter.sortFields));
        spnRequestSortField.setOnItemSelectedListener(this);
        spnRequestSortOrdering.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, FilterAdapter.sortOrderings));
        spnRequestSortOrdering.setOnItemSelectedListener(this);
        spnRequestSearchField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, OtherRequestAdapter.searchFields));
        spnRequestSearchField.setOnItemSelectedListener(this);
        RequestsActivity.getInstance().checkLocationEnabled(onLocationEnabled);
    }

    @Override
    public void onClick(View v)
    {
        if(v.getId() == R.id.btnViewRequest)
        {
            viewRequest();
        }
        else
        {
            parent.onClick(v);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        searchAndSortRequests();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {
        searchAndSortRequests();
    }

    @Override
    public void afterTextChanged(Editable s)
    {

    }

    private void viewRequest()
    {
        parent.request = (Request)lvRequests.getItemAtPosition(lvRequests.getCheckedItemPosition());
        if(parent.request == null)
        {
            tvRequests.requestFocus();
            tvRequests.setError(parent.getString(R.string.no_request_selected));
        }
        else
        {
            parent.vfAcceptedRequests.setDisplayedChild(1);
            parent.viewAcceptedRequestView.viewSelectedRequest();
        }
    }

    private void searchAndSortRequests()
    {
        lvRequests.clearChoices();
        OtherRequestAdapter otherRequestAdapter = (OtherRequestAdapter)lvRequests.getAdapter();
        if(otherRequestAdapter != null)
        {
            otherRequestAdapter.searchItems(spnRequestSearchField.getSelectedItem().toString(), etRequestSearchQuery.getText().toString());
            otherRequestAdapter.sortItems(spnRequestSortField.getSelectedItem().toString(), spnRequestSortOrdering.getSelectedItem().toString());
        }
    }

    protected void updateAcceptedRequests()
    {
        RequestsData.getAcceptedRequests(getAcceptedRequestsOnSuccess, getAcceptedRequestsOnError);
    }

    private final Runnable onLocationEnabled = new Runnable()
    {
        @Override
        public void run()
        {
            updateAcceptedRequests();
        }
    };

    private final ListRunnable<Request> getAcceptedRequestsOnSuccess = new ListRunnable<Request>()
    {
        @Override
        public void run(List<Request> requests)
        {
            lvRequests.setAdapter(new OtherRequestAdapter(parent, requests));
            tvNoRequestsYet.setVisibility(requests.isEmpty() ? View.VISIBLE : View.GONE);
            tvRequestListError.setVisibility(View.GONE);
            pbRequestList.setVisibility(View.GONE);
            btnViewRequest.setEnabled(true);
            searchAndSortRequests();
        }
    };

    private final SingleArgRunnable<String> getAcceptedRequestsOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            lvRequests.setAdapter(new OtherRequestAdapter(parent));
            tvNoRequestsYet.setVisibility(View.GONE);
            tvRequestListError.setText(text);
            tvRequestListError.setVisibility(View.VISIBLE);
            pbRequestList.setVisibility(View.GONE);
            btnViewRequest.setEnabled(true);
        }
    };

}
