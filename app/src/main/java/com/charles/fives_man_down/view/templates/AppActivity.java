package com.charles.fives_man_down.view.templates;

import android.app.Activity;
import android.content.Intent;
import android.view.inputmethod.InputMethodManager;
import com.charles.fives_man_down.model.runnable.SingleArgRunnable;
import com.charles.fives_man_down.processing.database.LoginData;
import com.charles.fives_man_down.view.views.main.LobbyActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public abstract class AppActivity extends Activity
{

    protected void goToLobby()
    {
        startActivity(new Intent(this, LobbyActivity.class));
    }

    public void hideKeyboard()
    {
        if(getCurrentFocus() != null)
        {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    protected void checkLoginState()
    {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if(firebaseUser == null)
        {
            finish();
        }
        else
        {
            LoginData.setEmail(firebaseUser.getEmail());
            LoginData.getUsernameFromEmail(LoginData.getEmail(), checkLoginOnSuccess, checkLoginOnError);
        }
    }

    private SingleArgRunnable<String> checkLoginOnSuccess = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            LoginData.setUsername(text);
        }
    };

    private SingleArgRunnable<String> checkLoginOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            FirebaseAuth.getInstance().signOut();
        }
    };

}
