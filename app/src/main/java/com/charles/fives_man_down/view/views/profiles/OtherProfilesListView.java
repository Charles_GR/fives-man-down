package com.charles.fives_man_down.view.views.profiles;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.adapters.FilterAdapter;
import com.charles.fives_man_down.model.adapters.ProfileAdapter;
import com.charles.fives_man_down.model.database.Profile;
import com.charles.fives_man_down.model.runnable.ListRunnable;
import com.charles.fives_man_down.model.runnable.SingleArgRunnable;
import com.charles.fives_man_down.processing.database.ProfilesData;
import java.util.List;

public class OtherProfilesListView implements OnClickListener, OnItemSelectedListener, TextWatcher
{

    private OtherProfilesActivity parent;
    private Button btnViewProfile;
    private EditText etProfileSearchQuery;
    private ListView lvProfiles;
    private ProgressBar pbProfileList;
    private Spinner spnProfileSortField;
    private Spinner spnProfileSortOrdering;
    private Spinner spnProfileSearchField;
    private TextView tvProfiles;
    private TextView tvNoProfilesYet;
    private TextView tvProfileListError;

    public OtherProfilesListView(OtherProfilesActivity parent)
    {
        this.parent = parent;
        btnViewProfile = (Button)parent.findViewById(R.id.btnViewProfile);
        etProfileSearchQuery = (EditText)parent.findViewById(R.id.etProfileSearchQuery);
        lvProfiles = (ListView)parent.findViewById(R.id.lvProfiles);
        pbProfileList = (ProgressBar)parent.findViewById(R.id.pbProfileList);
        spnProfileSortField = (Spinner)parent.findViewById(R.id.spnProfileSortField);
        spnProfileSortOrdering = (Spinner)parent.findViewById(R.id.spnProfileSortOrdering);
        spnProfileSearchField = (Spinner)parent.findViewById(R.id.spnProfileSearchField);
        tvProfiles = (TextView)parent.findViewById(R.id.tvProfiles);
        tvNoProfilesYet = (TextView)parent.findViewById(R.id.tvNoProfilesYet);
        tvProfileListError = (TextView)parent.findViewById(R.id.tvProfileListError);
        btnViewProfile.setOnClickListener(this);
        etProfileSearchQuery.addTextChangedListener(this);
        spnProfileSortField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, ProfileAdapter.sortFields));
        spnProfileSortField.setOnItemSelectedListener(this);
        spnProfileSortOrdering.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, FilterAdapter.sortOrderings));
        spnProfileSortOrdering.setOnItemSelectedListener(this);
        spnProfileSearchField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, ProfileAdapter.searchFields));
        spnProfileSearchField.setOnItemSelectedListener(this);
        ProfilesData.getProfiles(getProfilesOnSuccess, getProfilesOnError);
    }

    @Override
    public void onClick(View v)
    {
        if(v.getId() == R.id.btnViewProfile)
        {
            viewProfile();
        }
        else
        {
            parent.onClick(v);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        searchAndSortProfiles();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {
        searchAndSortProfiles();
    }


    @Override
    public void afterTextChanged(Editable s)
    {

    }

    private void searchAndSortProfiles()
    {
        lvProfiles.clearChoices();
        ProfileAdapter profileAdapter = (ProfileAdapter)lvProfiles.getAdapter();
        if(profileAdapter != null)
        {
            profileAdapter.searchItems(spnProfileSearchField.getSelectedItem().toString(), etProfileSearchQuery.getText().toString());
            profileAdapter.sortItems(spnProfileSortField.getSelectedItem().toString(), spnProfileSortOrdering.getSelectedItem().toString());
        }
    }

    private void viewProfile()
    {
        parent.selectedProfile = (Profile)lvProfiles.getItemAtPosition(lvProfiles.getCheckedItemPosition());
        if(parent.selectedProfile == null)
        {
            tvProfiles.requestFocus();
            tvProfiles.setError(parent.getString(R.string.no_profile_selected));
        }
        else
        {
            parent.vfOtherProfiles.setDisplayedChild(1);
            parent.viewOtherProfileView.viewProfile();
        }
    }

    private final ListRunnable<Profile> getProfilesOnSuccess = new ListRunnable<Profile>()
    {
        @Override
        public void run(List<Profile> profiles)
        {
            lvProfiles.setAdapter(new ProfileAdapter(parent, profiles));
            tvNoProfilesYet.setVisibility(profiles.isEmpty() ? View.VISIBLE : View.GONE);
            tvProfileListError.setVisibility(View.GONE);
            pbProfileList.setVisibility(View.GONE);
            btnViewProfile.setEnabled(true);
            searchAndSortProfiles();
        }
    };

    private final SingleArgRunnable<String> getProfilesOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            lvProfiles.setAdapter(new ProfileAdapter(parent));
            tvNoProfilesYet.setVisibility(View.GONE);
            tvProfileListError.setText(text);
            tvProfileListError.setVisibility(View.VISIBLE);
            pbProfileList.setVisibility(View.GONE);
            btnViewProfile.setEnabled(true);
        }
    };

}
