package com.charles.fives_man_down.view.views.options;

import android.os.Bundle;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.SeekBar.OnSeekBarChangeListener;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.MyApplication;
import com.charles.fives_man_down.model.runnable.SingleArgRunnable;
import com.charles.fives_man_down.processing.database.OptionsData;
import com.charles.fives_man_down.view.templates.AppNavActivity;

public class OptionsActivity extends AppNavActivity implements OnCheckedChangeListener, OnSeekBarChangeListener
{

    private EditText etNewRequestMaxDistance;
    private LinearLayout llNewRequestMaxDistance;
    private SeekBar sbNewRequestMaxDistance;
    private Switch swNewRequestCreated;
    private Switch swMyRequestAccepted;
    private Switch swMyRequestUnaccepted;
    private int previousNewRequestMaxDistanceValue = 20;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        etNewRequestMaxDistance = (EditText)findViewById(R.id.etNewRequestMaxDistance);
        llNewRequestMaxDistance = (LinearLayout)findViewById(R.id.llNewRequestMaxDistance);
        sbNewRequestMaxDistance = (SeekBar)findViewById(R.id.sbNewRequestMaxDistance);
        swNewRequestCreated = (Switch)findViewById(R.id.swNewRequestCreated);
        swMyRequestAccepted = (Switch)findViewById(R.id.swMyRequestAccepted);
        swMyRequestUnaccepted = (Switch)findViewById(R.id.swMyRequestUnaccepted);
        etNewRequestMaxDistance.setText(String.valueOf(sbNewRequestMaxDistance.getProgress() + 1));
        llNewRequestMaxDistance.setEnabled(swNewRequestCreated.isChecked());
        sbNewRequestMaxDistance.setOnSeekBarChangeListener(this);
        sbNewRequestMaxDistance.setProgress(MyApplication.getInstance().getOptions().getNewRequestMaxDistanceToNotify() - 1);
        swNewRequestCreated.setChecked(MyApplication.getInstance().getOptions().getNotifyOnNewRequestCreated());
        swNewRequestCreated.setOnCheckedChangeListener(this);
        swMyRequestAccepted.setChecked(MyApplication.getInstance().getOptions().getNotifyOnMyRequestAccepted());
        swMyRequestAccepted.setOnCheckedChangeListener(this);
        swMyRequestUnaccepted.setChecked(MyApplication.getInstance().getOptions().getNotifyOnMyRequestUnaccepted());
        swMyRequestUnaccepted.setOnCheckedChangeListener(this);
        tvActionBarTitle.setText(R.string.options_title);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
    {
        switch(buttonView.getId())
        {
            case R.id.swNewRequestCreated:
                llNewRequestMaxDistance.setEnabled(isChecked);
                OptionsData.setNotifyOnNewRequestCreatedValue(isChecked, new UpdateSwitchOnError(swNewRequestCreated));
                break;
            case R.id.swMyRequestAccepted:
                OptionsData.setNotifyOnMyRequestAcceptedValue(isChecked, new UpdateSwitchOnError(swMyRequestAccepted));
                break;
            case R.id.swMyRequestUnaccepted:
                OptionsData.setNotifyOnMyRequestUnacceptedValue(isChecked, new UpdateSwitchOnError(swMyRequestUnaccepted));
                break;
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
    {
        etNewRequestMaxDistance.setText(String.valueOf(progress + 1));
        OptionsData.setNewRequestMaxDistanceValue(progress + 1, new UpdateSeekBarOnError(sbNewRequestMaxDistance));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar)
    {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar)
    {

    }

    @Override
    protected void onUpButtonPressed()
    {
        goToLobby();
    }

    private void silentSetSwitchValue(Switch switchView, boolean checked)
    {
        switchView.setOnCheckedChangeListener(null);
        switchView.setChecked(checked);
        switchView.setOnCheckedChangeListener(this);
    }

    private void silentSetSeekBarValue(SeekBar seekBarView, int value)
    {
        seekBarView.setOnSeekBarChangeListener(null);
        seekBarView.setProgress(value);
        seekBarView.setOnSeekBarChangeListener(this);
    }

    private class UpdateSwitchOnError implements SingleArgRunnable<String>
    {

        private Switch switchView;

        private UpdateSwitchOnError(Switch switchView)
        {
            this.switchView = switchView;
        }

        @Override
        public void run(String text)
        {
            silentSetSwitchValue(switchView, !switchView.isChecked());
            Toast.makeText(OptionsActivity.this, text, Toast.LENGTH_SHORT).show();
        }

    }

    private class UpdateSeekBarOnError implements SingleArgRunnable<String>
    {

        private SeekBar seekBarView;

        private UpdateSeekBarOnError(SeekBar seekBarView)
        {
            this.seekBarView = seekBarView;
        }

        @Override
        public void run(String text)
        {
            silentSetSeekBarValue(seekBarView, previousNewRequestMaxDistanceValue);
            etNewRequestMaxDistance.setText(String.valueOf(seekBarView.getProgress() + 1));
            Toast.makeText(OptionsActivity.this, text, Toast.LENGTH_SHORT).show();
        }

    }

}
