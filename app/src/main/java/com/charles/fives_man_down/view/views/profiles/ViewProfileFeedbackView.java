package com.charles.fives_man_down.view.views.profiles;

import android.widget.EditText;
import com.charles.fives_man_down.R;

public class ViewProfileFeedbackView
{

    private OtherProfilesActivity parent;
    private EditText etRating;
    private EditText etPerceivedExperienceLevel;
    private EditText etText;
    private EditText etSubject;
    private EditText etAuthor;

    public ViewProfileFeedbackView(OtherProfilesActivity parent)
    {
        this.parent = parent;
        etRating = (EditText)parent.findViewById(R.id.etRating);
        etPerceivedExperienceLevel = (EditText)parent.findViewById(R.id.etPerceivedExperienceLevel);
        etText = (EditText)parent.findViewById(R.id.etTextB);
        etSubject = (EditText)parent.findViewById(R.id.etSubject);
        etAuthor = (EditText)parent.findViewById(R.id.etAuthor);
    }

    protected void viewFeedback()
    {
        etRating.setText(parent.selectedFeedback.getRating());
        etPerceivedExperienceLevel.setText(parent.selectedFeedback.getPerceivedExperienceLevel());
        etText.setText(parent.selectedFeedback.getText());
        etSubject.setText(parent.selectedFeedback.getSubject());
        etAuthor.setText(parent.selectedFeedback.getAuthor());
    }

}
