package com.charles.fives_man_down.view.views.requests;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.view.templates.TabbedActivity;

public class OtherRequestsActivity extends TabbedActivity implements OnClickListener
{

    private static OtherRequestsActivity instance;

    public static OtherRequestsActivity getInstance()
    {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_other_requests);
        super.onCreate(savedInstanceState);
        instance = this;
        tvActionBarTitle.setText(R.string.requests_title);
        createTab(getString(R.string.open_requests_title), OpenRequestsActivity.class);
        createTab(getString(R.string.accepted_requests_title), AcceptedRequestsActivity.class);
    }

    @Override
    public void onClick(View v)
    {
        RequestsActivity.getInstance().onClick(v);
    }

    @Override
    protected void onUpButtonPressed()
    {
        switch(thTabbedView.getCurrentTab())
        {
            case 0:
                OpenRequestsActivity.getInstance().onUpButtonPressed();
                break;
            case 1:
                AcceptedRequestsActivity.getInstance().onUpButtonPressed();
                break;
        }
    }

}
