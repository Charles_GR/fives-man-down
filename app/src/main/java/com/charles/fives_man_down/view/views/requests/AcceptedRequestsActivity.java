package com.charles.fives_man_down.view.views.requests;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.database.*;
import com.charles.fives_man_down.view.templates.*;

public class AcceptedRequestsActivity extends AppActivity implements OnClickListener
{

    private static AcceptedRequestsActivity instance;

    protected Request request;
    protected AcceptedRequestsListView acceptedRequestsListView;
    protected ViewAcceptedRequestView viewAcceptedRequestView;
    protected ViewFlipper vfAcceptedRequests;

    public static AcceptedRequestsActivity getInstance()
    {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_accepted_requests);
        super.onCreate(savedInstanceState);
        instance = this;
        acceptedRequestsListView = new AcceptedRequestsListView(this);
        viewAcceptedRequestView = new ViewAcceptedRequestView(this);
        vfAcceptedRequests = (ViewFlipper)findViewById(R.id.vfAcceptedRequests);
    }

    @Override
    public void onClick(View v)
    {
        RequestsActivity.getInstance().onClick(v);
    }

    public void onLocationChanged()
    {
        acceptedRequestsListView.updateAcceptedRequests();
    }

    public void onUpButtonPressed()
    {
        switch(vfAcceptedRequests.getDisplayedChild())
        {
            case 0:
                goToLobby();
                break;
            case 1:
                vfAcceptedRequests.setDisplayedChild(0);
                break;
        }
    }

}

