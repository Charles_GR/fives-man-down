package com.charles.fives_man_down.view.views.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.view.templates.AppMainActivity;
import com.charles.fives_man_down.view.views.account.AccountActivity;
import com.charles.fives_man_down.view.views.profiles.ProfilesActivity;
import com.charles.fives_man_down.view.views.requests.RequestsActivity;
import com.charles.fives_man_down.view.views.options.OptionsActivity;

public class LobbyActivity extends AppMainActivity
{

    private LinearLayout llProfiles;
    private LinearLayout llRequests;
    private LinearLayout llOptions;
    private LinearLayout llAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lobby);
        llProfiles = (LinearLayout)findViewById(R.id.llProfiles);
        llRequests = (LinearLayout)findViewById(R.id.llRequests);
        llOptions = (LinearLayout)findViewById(R.id.llOptions);
        llAccount = (LinearLayout)findViewById(R.id.llAccount);
        llProfiles.setOnClickListener(this);
        llRequests.setOnClickListener(this);
        llOptions.setOnClickListener(this);
        llAccount.setOnClickListener(this);
        tvActionBarTitle.setText(R.string.lobby_title);
    }

    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.llProfiles:
                startActivity(new Intent(this, ProfilesActivity.class));
                break;
            case R.id.llRequests:
                startActivity(new Intent(this, RequestsActivity.class));
                break;
            case R.id.llOptions:
                startActivity(new Intent(this, OptionsActivity.class));
                break;
            case R.id.llAccount:
                startActivity(new Intent(this, AccountActivity.class));
                break;
            default:
                super.onClick(v);
                break;
        }
    }

}
