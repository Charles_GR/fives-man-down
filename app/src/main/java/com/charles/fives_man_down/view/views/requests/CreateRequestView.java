package com.charles.fives_man_down.view.views.requests;

import android.app.*;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.database.*;
import com.charles.fives_man_down.model.runnable.SingleArgRunnable;
import com.charles.fives_man_down.processing.database.*;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CreateRequestView implements OnClickListener, OnDateSetListener, OnTimeSetListener
{

    private static Position position;
    private static Calendar cal;
    private static final int PLACE_PICKER_REQUEST = 1;

    private MyRequestsActivity parent;
    private Button btnSelectLocation;
    private Button btnSelectStartTime;
    private Button btnSave;
    private Button btnCancel;
    private EditText etDescription;
    private EditText etPlayersNeeded;
    private EditText etLocation;
    private EditText etStartTime;
    private ProgressBar pbProgress;
    private Spinner spnMinExperienceLevel;
    private Spinner spnMaxExperienceLevel;
    private TextView tvMinExperienceLevel;
    private TextView tvMaxExperienceLevel;
    private TextView tvLocation;
    private TextView tvStartTime;

    public CreateRequestView(MyRequestsActivity parent)
    {
        this.parent = parent;
        btnSelectLocation = (Button)parent.findViewById(R.id.btnSelectLocation);
        btnSelectStartTime = (Button)parent.findViewById(R.id.btnSelectStartTime);
        btnSave = (Button)parent.findViewById(R.id.btnSave);
        btnCancel = (Button)parent.findViewById(R.id.btnCancel);
        etDescription = (EditText)parent.findViewById(R.id.etDescriptionA);
        etPlayersNeeded = (EditText)parent.findViewById(R.id.etPlayersNeededA);
        etLocation = (EditText)parent.findViewById(R.id.etLocationA);
        etStartTime = (EditText)parent.findViewById(R.id.etStartTimeA);
        pbProgress = (ProgressBar)parent.findViewById(R.id.pbProgress);
        spnMinExperienceLevel = (Spinner)parent.findViewById(R.id.spnMinExperienceLevel);
        spnMaxExperienceLevel = (Spinner)parent.findViewById(R.id.spnMaxExperienceLevel);
        tvMinExperienceLevel = (TextView)parent.findViewById(R.id.tvMinExperienceLevel);
        tvMaxExperienceLevel = (TextView)parent.findViewById(R.id.tvMaxExperienceLevel);
        tvLocation = (TextView)parent.findViewById(R.id.tvLocation);
        tvStartTime = (TextView)parent.findViewById(R.id.tvStartTime);
        btnSelectLocation.setOnClickListener(this);
        btnSelectStartTime.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        spnMinExperienceLevel.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, Request.experienceLevels));
        spnMaxExperienceLevel.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, Request.experienceLevels));
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnSelectLocation:
                try
                {
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                    RequestsActivity.getInstance().startActivityForResult(builder.build(RequestsActivity.getInstance()), PLACE_PICKER_REQUEST);
                }
                catch(GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException ex)
                {

                }
                break;
            case R.id.btnSelectStartTime:
                new DatePickerDialog(parent, this, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.btnSave:
                saveRequest();
                break;
            case R.id.btnCancel:
                parent.vfMyRequests.setDisplayedChild(0);
                break;
            default:
                parent.onClick(v);
                break;
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode == PLACE_PICKER_REQUEST && resultCode == Activity.RESULT_OK)
        {
            LatLng latLng = PlacePicker.getPlace(parent, data).getLatLng();
            position = new Position(latLng.latitude, latLng.longitude);
            etLocation.setText(position.toString());
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth)
    {
        cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        etStartTime.setText(SimpleDateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(cal.getTime()));
        new TimePickerDialog(parent, this, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), true).show();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute)
    {
        cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        etStartTime.setText(SimpleDateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(cal.getTime()));
    }

    private void saveRequest()
    {
        clearErrors();
        if(validInputs())
        {
            setActionInProgress();
            RequestsData.saveRequest(etDescription.getText().toString(), Integer.parseInt(etPlayersNeeded.getText().toString()), spnMinExperienceLevel.getSelectedItem().toString(),
                    spnMaxExperienceLevel.getSelectedItem().toString(), position, cal.getTime(), saveRequestOnSuccess, saveRequestOnError);
            parent.vfMyRequests.setDisplayedChild(0);
        }

    }

    private void clearErrors()
    {
        etDescription.setError(null);
        etPlayersNeeded.setError(null);
        tvMinExperienceLevel.setError(null);
        tvMaxExperienceLevel.setError(null);
        etLocation.setError(null);
    }

    protected void clearForm()
    {
        clearErrors();
        etDescription.setText("");
        etPlayersNeeded.setText("");
        spnMinExperienceLevel.setSelection(0);
        spnMaxExperienceLevel.setSelection(0);
        etLocation.setText("");
        etStartTime.setText("");
    }

    private boolean validInputs()
    {
        if(etDescription.getText().toString().isEmpty())
        {
            etDescription.requestFocus();
            etDescription.setError(parent.getString(R.string.description_empty));
            return false;
        }
        if(etPlayersNeeded.getText().toString().isEmpty())
        {
            etPlayersNeeded.requestFocus();
            etPlayersNeeded.setError(parent.getString(R.string.players_needed_empty));
            return false;
        }
        if(Integer.valueOf(etPlayersNeeded.getText().toString()) == 0 || Integer.valueOf(etPlayersNeeded.getText().toString()) > 4)
        {
            etPlayersNeeded.requestFocus();
            etPlayersNeeded.setError(parent.getString(R.string.players_needed_invalid));
            return false;
        }
        if(spnMinExperienceLevel.getSelectedItemPosition() > spnMaxExperienceLevel.getSelectedItemPosition())
        {
            tvMinExperienceLevel.requestFocus();
            tvMinExperienceLevel.setError(parent.getString(R.string.min_experience_level_more_than_max));
            return false;
        }
        if(etLocation.getText().toString().isEmpty())
        {
            tvLocation.requestFocus();
            tvLocation.setError(parent.getString(R.string.location_empty));
            return false;
        }
        if(etStartTime.getText().toString().isEmpty())
        {
            tvStartTime.requestFocus();
            tvStartTime.setError(parent.getString(R.string.start_time_empty));
            return false;
        }
        if(cal.getTimeInMillis() - new Date().getTime() < 1200000)
        {
            tvStartTime.requestFocus();
            tvStartTime.setError(parent.getString(R.string.start_time_too_soon));
            return false;
        }
        return true;
    }

    private void setActionInProgress()
    {
        btnSelectLocation.setEnabled(false);
        btnSelectStartTime.setEnabled(false);
        btnSave.setEnabled(false);
        btnCancel.setEnabled(false);
        etDescription.setEnabled(false);
        etPlayersNeeded.setEnabled(false);
        spnMinExperienceLevel.setEnabled(false);
        spnMaxExperienceLevel.setEnabled(false);
        pbProgress.setVisibility(View.VISIBLE);
    }

    private void setActionComplete()
    {
        btnSelectLocation.setEnabled(true);
        btnSelectStartTime.setEnabled(true);
        btnSave.setEnabled(true);
        btnCancel.setEnabled(true);
        etDescription.setEnabled(true);
        etPlayersNeeded.setEnabled(true);
        spnMinExperienceLevel.setEnabled(true);
        spnMaxExperienceLevel.setEnabled(true);
        pbProgress.setVisibility(View.GONE);
    }

    private final Runnable saveRequestOnSuccess = new Runnable()
    {
        @Override
        public void run()
        {
            Toast.makeText(parent, R.string.request_successfully_saved, Toast.LENGTH_SHORT).show();
            setActionComplete();
        }
    };

    private final SingleArgRunnable<String> saveRequestOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            Toast.makeText(parent, text, Toast.LENGTH_SHORT).show();
            setActionComplete();
        }
    };

}
