package com.charles.fives_man_down.view.views.profiles;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.processing.database.ProfilesData;
import com.charles.fives_man_down.processing.media.ImageParser;

public class ViewOtherProfileView implements OnClickListener, OnItemSelectedListener, TextWatcher
{

    private OtherProfilesActivity parent;
    private Button btnViewFeedback;
    private EditText etFullName;
    private EditText etBio;
    private EditText etExperienceLevel;
    private ImageView ivPhoto;

    public ViewOtherProfileView(OtherProfilesActivity parent)
    {
        this.parent = parent;
        btnViewFeedback = (Button)parent.findViewById(R.id.btnViewFeedback);
        etFullName = (EditText)parent.findViewById(R.id.etFullName);
        etBio = (EditText)parent.findViewById(R.id.etBio);
        etExperienceLevel = (EditText)parent.findViewById(R.id.etExperienceLevel);
        ivPhoto = (ImageView)parent.findViewById(R.id.ivPhoto);
        btnViewFeedback.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if(v.getId() == R.id.btnViewFeedback)
        {
            viewProfileFeedback();
        }
        else
        {
            parent.onClick(v);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {

    }

    @Override
    public void afterTextChanged(Editable s)
    {

    }

    protected void viewProfile()
    {
        etFullName.setText(parent.selectedProfile.getFullName());
        etBio.setText(parent.selectedProfile.getBio());
        ivPhoto.setImageBitmap(parent.selectedProfile.getPhoto() == null ? null : ImageParser.decodeImage(parent.selectedProfile.getPhoto()));
        etExperienceLevel.setText(parent.selectedProfile.getExperienceLevel());
    }

    private void viewProfileFeedback()
    {
        ProfilesData.getProfileFeedback(parent.selectedProfile.getUsername(), ProfileFeedbackListView.getInstance().getFeedbackOnSuccess,
                ProfileFeedbackListView.getInstance().getFeedbackOnError);
        parent.profileFeedbackListView.tvFeedback.setText(parent.getResources().getString(R.string.feedback_label_for, parent.selectedProfile.getUsername()));
        parent.vfOtherProfiles.setDisplayedChild(2);
    }

}
