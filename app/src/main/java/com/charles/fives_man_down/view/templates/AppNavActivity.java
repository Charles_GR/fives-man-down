package com.charles.fives_man_down.view.templates;

import android.os.Bundle;
import android.view.*;
import android.widget.TextView;
import android.widget.Toast;
import com.charles.fives_man_down.R;

public abstract class AppNavActivity extends AppMainActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        if(getActionBar() != null)
        {
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setDisplayShowCustomEnabled(true);
            View customView = LayoutInflater.from(this).inflate(R.layout.action_layout_nav, null);
            getActionBar().setCustomView(customView);
            tvActionBarTitle = (TextView)customView.findViewById(R.id.tvActionBarTitle);
            customView.findViewById(R.id.ivHome).setOnClickListener(this);
            customView.findViewById(R.id.ivLogout).setOnClickListener(this);
            customView.findViewById(R.id.ivHome).setOnLongClickListener(this);
            customView.findViewById(R.id.ivLogout).setOnLongClickListener(this);
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onClick(View v)
    {
        if(v.getId() == R.id.ivHome)
        {
            goToLobby();
        }
        else
        {
            super.onClick(v);
        }
    }

    @Override
    public boolean onLongClick(View v)
    {
        if(v.getId() == R.id.ivHome)
        {
            Toast.makeText(this, R.string.home, Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onLongClick(v);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            onUpButtonPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected abstract void onUpButtonPressed();

}
