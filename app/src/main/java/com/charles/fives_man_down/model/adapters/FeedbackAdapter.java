package com.charles.fives_man_down.model.adapters;

import android.content.Context;
import android.view.*;
import android.widget.TextView;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.database.Feedback;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class FeedbackAdapter extends FilterAdapter<Feedback>
{

    public static final String[] sortFields = new String[]{"Rating", "Perceived Exp Level", "Author", "Date Created"};
    public static final String[] searchFields = new String[]{"Perceived Exp Level", "Text", "Author"};

    public FeedbackAdapter(Context context, List<Feedback> feedback)
    {
        super(context, R.layout.list_item_feedback, feedback);
        setComparator(new FeedbackComparator());
    }

    public FeedbackAdapter(Context context)
    {
        super(context, R.layout.list_item_feedback, new ArrayList<Feedback>());
        setComparator(new FeedbackComparator());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_feedback, parent, false);
        }
        Feedback feedback = getItem(position);
        TextView tvRating = (TextView)convertView.findViewById(R.id.tvRatingValue);
        TextView tvPerceivedExperienceLevel = (TextView)convertView.findViewById(R.id.tvPerceivedExperienceLevelValue);
        TextView tvAuthor = (TextView)convertView.findViewById(R.id.tvAuthorValue);
        TextView tvDateCreated = (TextView)convertView.findViewById(R.id.tvDateCreatedValue);
        tvRating.setText(feedback.getRating());
        tvPerceivedExperienceLevel.setText(feedback.getPerceivedExperienceLevel());
        tvAuthor.setText(feedback.getAuthor());
        tvDateCreated.setText(SimpleDateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(feedback.getDateCreated()));
        return convertView;
    }

    @Override
    protected void performSearch(String searchField, String searchQuery)
    {
        for(Feedback feedback : items)
        {
            switch(searchField)
            {
                case "Perceived Exp Level":
                    if(feedback.getPerceivedExperienceLevel().contains(searchQuery))
                    {
                        filteredItems.add(feedback);
                    }
                    break;
                case "Text":
                    if(feedback.getText().contains(searchQuery))
                    {
                        filteredItems.add(feedback);
                    }
                    break;
                case "Author":
                    if(feedback.getAuthor().contains(searchQuery))
                    {
                        filteredItems.add(feedback);
                    }
                    break;
            }
        }
    }

    private class FeedbackComparator extends ItemComparator
    {

        @Override
        public int compare(Feedback feedbackA, Feedback feedbackB)
        {
            int compare = 0;
            switch(sortField)
            {
                case "Rating":
                    compare = feedbackA.getRating().compareTo(feedbackB.getRating());
                    break;
                case "Perceived Exp Level":
                    compare = feedbackA.getPerceivedExperienceLevel().compareTo(feedbackB.getPerceivedExperienceLevel());
                    break;
                case "Author":
                    compare = feedbackA.getAuthor().compareTo(feedbackB.getAuthor());
                    break;
                case "Date Created":
                    compare = feedbackA.getDateCreated().compareTo(feedbackB.getDateCreated());
                    break;
            }
            return sortOrdering.equals("Ascending") ? compare : -1 * compare;
        }

    }

}
