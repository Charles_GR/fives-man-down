package com.charles.fives_man_down.model;

import android.app.Application;
import android.location.Location;
import android.widget.Toast;

import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.database.Options;
import com.charles.fives_man_down.model.database.Position;
import com.charles.fives_man_down.model.runnable.SingleArgRunnable;
import com.charles.fives_man_down.processing.database.OptionsData;

public class MyApplication extends Application
{

    private static MyApplication instance;
    private Options options;
    private Position userPosition;

    public static MyApplication getInstance()
    {
        return instance;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        instance = this;
    }

    public Options getOptions()
    {
        return options;
    }

    public Position getUserPosition()
    {
        return userPosition;
    }

    public void setUserPosition(Location location)
    {
        userPosition = new Position(location.getLatitude(), location.getLongitude());
    }

    public void fetchOptions()
    {
        OptionsData.getOptions(getOptionsOnSuccess, getOptionsOnError);
    }

    private final SingleArgRunnable<Options> getOptionsOnSuccess = new SingleArgRunnable<Options>()
    {
        @Override
        public void run(Options options)
        {
            instance.options = options == null ? new Options() : options;
        }
    };

    private final SingleArgRunnable<String> getOptionsOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            Toast.makeText(MyApplication.instance, R.string.failed_to_load_options, Toast.LENGTH_SHORT).show();
        }
    };

}
