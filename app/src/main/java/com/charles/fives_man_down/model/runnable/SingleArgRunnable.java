package com.charles.fives_man_down.model.runnable;

public interface SingleArgRunnable<T>
{

    void run(T arg);

}
