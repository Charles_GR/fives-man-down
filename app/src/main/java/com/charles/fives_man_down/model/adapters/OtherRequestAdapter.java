package com.charles.fives_man_down.model.adapters;

import android.content.Context;
import android.view.*;
import android.widget.TextView;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.database.Request;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class OtherRequestAdapter extends FilterAdapter<Request>
{

    public static final String[] searchFields = new String[]{"Description", "Requester"};
    public static final String[] sortFields = new String[]{"Players Needed", "Distance", "Start Time", "Requester", "Date Created"};

    public OtherRequestAdapter(Context context, List<Request> requests)
    {
        super(context, R.layout.list_item_other_request, requests);
        setComparator(new RequestComparator());
    }

    public OtherRequestAdapter(Context context)
    {
        super(context, R.layout.list_item_other_request, new ArrayList<Request>());
        setComparator(new RequestComparator());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_other_request, parent, false);
        }
        Request request = getItem(position);
        TextView tvPlayersNeeded = (TextView)convertView.findViewById(R.id.tvPlayersNeededValue);
        TextView tvDistance = (TextView)convertView.findViewById(R.id.tvDistanceValue);
        TextView tvStartTime = (TextView)convertView.findViewById(R.id.tvStartTimeValue);
        TextView tvRequester = (TextView)convertView.findViewById(R.id.tvRequesterValue);
        TextView tvDateCreated = (TextView)convertView.findViewById(R.id.tvDateCreatedValue);
        tvPlayersNeeded.setText(String.valueOf(request.getPlayersNeeded()));
        Double distance = request.calcDistanceToPosition();
        tvDistance.setText(distance == null ? "?" : String.valueOf(distance));
        tvStartTime.setText(SimpleDateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(request.getStartTime()));
        tvRequester.setText(request.getRequester());
        tvDateCreated.setText(SimpleDateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(request.getDateCreated()));
        return convertView;
    }

    @Override
    protected void performSearch(String searchField, String searchQuery)
    {
        for(Request request : items)
        {
            switch(searchField)
            {
                case "Description":
                    if(request.getDescription().contains(searchQuery))
                    {
                        filteredItems.add(request);
                    }
                    break;
                case "Requester":
                    if(request.getRequester().contains(searchQuery))
                    {
                        filteredItems.add(request);
                    }
                    break;
            }
        }
    }

    private class RequestComparator extends ItemComparator
    {

        @Override
        public int compare(Request requestA, Request requestB)
        {
            int compare = 0;
            switch(sortField)
            {
                case "Players Needed":
                    compare = Integer.valueOf(requestA.getPlayersNeeded()).compareTo(requestB.getPlayersNeeded());
                    break;
                case "Distance":
                    compare = requestA.calcDistanceToPosition().compareTo(requestB.calcDistanceToPosition());
                    break;
                case "Start Time":
                    compare = requestA.getStartTime().compareTo(requestB.getStartTime());
                    break;
                case "Requester":
                    compare = requestA.getRequester().compareTo(requestB.getRequester());
                    break;
                case "Date Created":
                    compare = requestA.getDateCreated().compareTo(requestB.getDateCreated());
                    break;
            }
            return sortOrdering.equals("Ascending") ? compare : -1 * compare;
        }

    }

}
