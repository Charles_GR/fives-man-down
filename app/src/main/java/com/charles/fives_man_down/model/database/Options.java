package com.charles.fives_man_down.model.database;

public class Options
{

    private boolean notifyOnNewRequestCreated;
    private boolean notifyOnMyRequestAccepted;
    private boolean notifyOnMyRequestUnaccepted;
    private int newRequestMaxDistanceToNotify;

    public Options()
    {
        notifyOnNewRequestCreated = true;
        notifyOnMyRequestAccepted = true;
        notifyOnMyRequestUnaccepted = true;
        newRequestMaxDistanceToNotify = 20;
    }

    public boolean getNotifyOnNewRequestCreated()
    {
        return notifyOnNewRequestCreated;
    }

    public boolean getNotifyOnMyRequestAccepted()
    {
        return notifyOnMyRequestAccepted;
    }

    public boolean getNotifyOnMyRequestUnaccepted()
    {
        return notifyOnMyRequestUnaccepted;
    }

    public int getNewRequestMaxDistanceToNotify()
    {
        return newRequestMaxDistanceToNotify;
    }

    public void setNotifyOnNewRequestCreated(boolean notifyOnNewRequestCreated)
    {
        this.notifyOnNewRequestCreated = notifyOnNewRequestCreated;
    }

    public void setNotifyOnMyRequestAccepted(boolean notifyOnMyRequestAccepted)
    {
        this.notifyOnMyRequestAccepted = notifyOnMyRequestAccepted;
    }

    public void setNotifyOnMyRequestUnaccepted(boolean notifyOnMyRequestUnaccepted)
    {
        this.notifyOnMyRequestUnaccepted = notifyOnMyRequestUnaccepted;
    }

    public void setNewRequestMaxDistanceToNotify(int newRequestMaxDistanceToNotify)
    {
        this.newRequestMaxDistanceToNotify = newRequestMaxDistanceToNotify;
    }

}
