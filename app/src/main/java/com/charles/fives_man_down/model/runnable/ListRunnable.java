package com.charles.fives_man_down.model.runnable;

import java.util.List;

public interface ListRunnable<T> extends SingleArgRunnable<List<T>>
{

}
