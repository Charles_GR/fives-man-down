package com.charles.fives_man_down.model.database;

import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.MyApplication;

import java.util.*;
import java.util.Map.Entry;

public class Profile
{

    private String username;
    private String fullName;
    private String bio;
    private String photo;
    private String experienceLevel;
    private HashMap<String, Feedback> feedback;

    public Profile()
    {
        feedback = new HashMap<>();
    }

    public Profile(String username, String fullName, String bio, String photo, String experienceLevel)
    {
        this.username = username;
        this.fullName = fullName;
        this.bio = bio;
        this.photo = photo;
        this.experienceLevel = experienceLevel;
        feedback = new HashMap<>();
    }

    public String getUsername()
    {
        return username;
    }

    public String getFullName()
    {
        return fullName;
    }

    public String getBio()
    {
        return bio;
    }

    public String getPhoto()
    {
        return photo;
    }

    public String getExperienceLevel()
    {
        return experienceLevel;
    }

    public HashMap<String, Feedback> getFeedback()
    {
        return feedback;
    }

    public String calcAverageRating()
    {
        int totalRating = 0;
        if(feedback.isEmpty())
        {
            return MyApplication.getInstance().getResources().getString(R.string.unavailable);
        }
        for(Entry<String, Feedback> entry : feedback.entrySet())
        {
            totalRating += entry.getValue().getRating().length();
        }
        int averageRating = Math.round((float)totalRating / (float)feedback.size());
        return new String(new char[averageRating]).replace("\0", "★");
    }

}
