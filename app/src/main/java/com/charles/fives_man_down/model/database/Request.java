package com.charles.fives_man_down.model.database;

import com.charles.fives_man_down.model.MyApplication;
import java.util.*;

public class Request
{

    public static final String[] experienceLevels = Feedback.experienceLevels;

    private String key;
    private String description;
    private int playersNeeded;
    private String minExperienceLevel;
    private String maxExperienceLevel;
    private Position position;
    private Date startTime;
    private String requester;
    private Date dateCreated;
    private List<String> acceptees;

    public Request()
    {
        acceptees = new ArrayList<>();
    }

    public Request(String key, String description, int playersNeeded, String minExperienceLevel, String maxExperienceLevel, Position position, Date startTime, String requester)
    {
        this.key = key;
        this.description = description;
        this.playersNeeded = playersNeeded;
        this.minExperienceLevel = minExperienceLevel;
        this.maxExperienceLevel = maxExperienceLevel;
        this.position = position;
        this.startTime = startTime;
        this.requester = requester;
        dateCreated = new Date();
        acceptees = new ArrayList<>();
    }

    public String getKey()
    {
        return key;
    }

    public String getDescription()
    {
        return description;
    }

    public int getPlayersNeeded()
    {
        return playersNeeded;
    }

    public String getMinExperienceLevel()
    {
        return minExperienceLevel;
    }

    public String getMaxExperienceLevel()
    {
        return maxExperienceLevel;
    }

    public Position getPosition()
    {
        return position;
    }

    public Date getStartTime()
    {
        return startTime;
    }

    public String getRequester()
    {
        return requester;
    }

    public Date getDateCreated()
    {
        return dateCreated;
    }

    public List<String> getAcceptees()
    {
        return acceptees;
    }

    public Double calcDistanceToPosition()
    {
        if(MyApplication.getInstance().getUserPosition() == null)
        {
            return null;
        }
        return MyApplication.getInstance().getUserPosition().calcDistanceTo(position);
    }

    public boolean hasExpired()
    {
        return startTime.compareTo(new Date()) < 0;
    }

    public boolean isAcceptedBy(String username)
    {
        return acceptees != null && acceptees.contains(username);
    }

    public boolean profileCanAcceptRequest(Profile profile)
    {
        List<String> experienceLevels = Arrays.asList(Request.experienceLevels);
        return experienceLevels.indexOf(profile.getExperienceLevel()) >= experienceLevels.indexOf(minExperienceLevel) &&
                experienceLevels.indexOf(profile.getExperienceLevel()) <= experienceLevels.indexOf(maxExperienceLevel);
    }

}
