package com.charles.fives_man_down.model.runnable;

public interface DoubleArgRunnable<Ta, Tb>
{

    void run(Ta argA, Tb argB);

}

