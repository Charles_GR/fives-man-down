package com.charles.fives_man_down.model.database;

import java.util.Date;

public class Feedback
{

    public static final String[] ratings = new String[]{"★", "★★", "★★★", "★★★★", "★★★★★"};
    public static final String[] experienceLevels = {"Beginner", "Amateur", "Intermediate", "Expert", "Professional"};

    private String rating;
    private String perceivedExperienceLevel;
    private String text;
    private String subject;
    private String author;
    private Date dateCreated;

    public Feedback()
    {

    }

    public Feedback(String rating, String perceivedExperienceLevel, String text, String subject, String author)
    {
        this.rating = rating;
        this.perceivedExperienceLevel = perceivedExperienceLevel;
        this.text = text;
        this.subject = subject;
        this.author = author;
        this.dateCreated = new Date();
    }

    public String getRating()
    {
        return rating;
    }

    public String getPerceivedExperienceLevel()
    {
        return perceivedExperienceLevel;
    }

    public String getText()
    {
        return text;
    }

    public String getSubject()
    {
        return subject;
    }

    public String getAuthor()
    {
        return author;
    }

    public Date getDateCreated()
    {
        return dateCreated;
    }

}
