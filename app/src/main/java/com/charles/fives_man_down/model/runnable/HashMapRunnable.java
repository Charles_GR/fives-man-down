package com.charles.fives_man_down.model.runnable;

import java.util.HashMap;

public interface HashMapRunnable<K, V> extends SingleArgRunnable<HashMap<K, V>>
{

}
