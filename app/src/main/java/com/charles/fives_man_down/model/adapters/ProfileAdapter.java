package com.charles.fives_man_down.model.adapters;

import android.content.Context;
import android.view.*;
import android.widget.ImageView;
import android.widget.TextView;
import com.charles.fives_man_down.R;
import com.charles.fives_man_down.model.database.Profile;
import com.charles.fives_man_down.processing.media.ImageParser;
import java.util.*;

public class ProfileAdapter extends FilterAdapter<Profile>
{

    public static final String [] sortFields = new String[]{ "Username", "Full Name", "Experience Level", "Average Rating"};
    public static final String [] searchFields = new String[]{ "Username", "Full Name", "Bio", "Experience Level"};

    public ProfileAdapter(Context context, List<Profile> profiles)
    {
        super(context, R.layout.list_item_profile, profiles);
        setComparator(new ProfileComparator());
    }

    public ProfileAdapter(Context context)
    {
        super(context, R.layout.list_item_profile, new ArrayList<Profile>());
        setComparator(new ProfileComparator());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_profile, parent, false);
        }
        Profile profile = getItem(position);
        TextView tvUserName = (TextView)convertView.findViewById(R.id.tvUsernameValue);
        TextView tvFullName = (TextView)convertView.findViewById(R.id.tvFullNameValue);
        TextView tvExperienceLevel = (TextView)convertView.findViewById(R.id.tvExperienceLevelValue);
        TextView tvAverageRating = (TextView)convertView.findViewById(R.id.tvAverageRatingValue);
        ImageView ivPhoto = (ImageView)convertView.findViewById(R.id.ivPhoto);
        tvUserName.setText(profile.getUsername());
        tvFullName.setText(profile.getFullName());
        tvExperienceLevel.setText(profile.getExperienceLevel());
        tvAverageRating.setText(profile.calcAverageRating());
        ivPhoto.setImageBitmap(profile.getPhoto() == null ? null : ImageParser.decodeImage(profile.getPhoto()));
        return convertView;
    }

    @Override
    protected void performSearch(String searchField, String searchQuery)
    {
        for(Profile profile : items)
        {
            switch(searchField)
            {
                case "Username":
                    if(profile.getUsername().contains(searchQuery))
                    {
                        filteredItems.add(profile);
                    }
                    break;
                case "Full Name":
                    if(profile.getFullName().contains(searchQuery))
                    {
                        filteredItems.add(profile);
                    }
                    break;
                case "Bio":
                    if(profile.getBio().contains(searchQuery))
                    {
                        filteredItems.add(profile);
                    }
                    break;
                case "Experience Level":
                    if(profile.getExperienceLevel().contains(searchQuery))
                    {
                        filteredItems.add(profile);
                    }
                    break;
            }
        }
    }

    private class ProfileComparator extends ItemComparator
    {

        @Override
        public int compare(Profile profileA, Profile profileB)
        {
            int compare = 0;
            switch(sortField)
            {
                case "Username":
                    compare = profileA.getUsername().compareTo(profileB.getUsername());
                    break;
                case "Full Name":
                    compare = profileA.getFullName().compareTo(profileB.getFullName());
                    break;
                case "Experience Level":
                    compare = profileA.getExperienceLevel().compareTo(profileB.getExperienceLevel());
                    break;
                case "Average Rating":
                    compare = profileA.calcAverageRating().compareTo(profileB.calcAverageRating());
                    break;
            }
            return sortOrdering.equals("Ascending") ? compare : -1 * compare;
        }

    }

}