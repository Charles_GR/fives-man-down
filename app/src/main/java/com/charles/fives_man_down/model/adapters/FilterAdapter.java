package com.charles.fives_man_down.model.adapters;

import android.content.Context;
import android.widget.ArrayAdapter;
import java.util.*;

public abstract class FilterAdapter<T> extends ArrayAdapter<T>
{

    public static final String[] sortOrderings = new String[]{"Ascending", "Descending"};
    protected List<T> items;
    protected List<T> filteredItems;
    protected ItemComparator comparator;

    protected FilterAdapter(Context context, int resource, List<T> items)
    {
        super(context, resource, items);
        this.items = new ArrayList<>(items);
        this.filteredItems = items;
    }

    public void setComparator(ItemComparator comparator)
    {
        this.comparator = comparator;
    }

    public void sortItems(String sortField, String sortOrdering)
    {
        comparator.setSortField(sortField);
        comparator.setSortOrdering(sortOrdering);
        Collections.sort(filteredItems, comparator);
        notifyDataSetChanged();
    }

    public void searchItems(String searchField, String searchQuery)
    {
        filteredItems.clear();
        if(searchQuery.isEmpty())
        {
            filteredItems.addAll(items);
        }
        else
        {
            performSearch(searchField, searchQuery);
        }
        notifyDataSetChanged();
    }

    protected abstract void performSearch(String searchField, String searchQuery);

    protected abstract class ItemComparator implements Comparator<T>
    {

        protected String sortField;
        protected String sortOrdering;

        public void setSortField(String sortField)
        {
            this.sortField = sortField;
        }

        public void setSortOrdering(String sortOrdering)
        {
            this.sortOrdering = sortOrdering;
        }

    }

}

