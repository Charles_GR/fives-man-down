package com.charles.fives_man_down.model.database;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Notification
{

    public static final String NEW_PLAYER_REQUEST = "New Player Request";
    public static final String PLAYER_REQUEST_ACCEPTED = "Player Request Accepted";
    public static final String PLAYER_REQUEST_UNACCEPTED = "Player Request Unaccepted";
    private static final String EXPAND_FOR_MORE_DETAILS = "Expand for more details.";

    private String title;
    private Date time;
    private Request request;
    private String data;

    public Notification()
    {

    }

    public Notification(String title, Request request, String data)
    {
        this.title = title;
        time = new Date();
        this.request = request;
        this.data = data;
    }

    public String getTitle()
    {
        return title;
    }

    public Date getTime()
    {
        return time;
    }

    public Request getRequest()
    {
        return request;
    }

    public String getData()
    {
        return data;
    }

    public String createMessage()
    {
        switch(title)
        {
            case NEW_PLAYER_REQUEST:
                return EXPAND_FOR_MORE_DETAILS;
            case PLAYER_REQUEST_ACCEPTED:
                return "Accepted By: " + data;
            case PLAYER_REQUEST_UNACCEPTED:
                return "Unaccepted By: " + data;
            default:
                return null;
        }
    }

    public String createBigMessage()
    {
        switch(title)
        {
            case NEW_PLAYER_REQUEST:
                return "Requester: " + request.getRequester() + "\n" + "Distance: " + request.calcDistanceToPosition() + " miles" + "\n" + "Start Time: " +
                        SimpleDateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(request.getStartTime());
            default:
                return null;
        }
    }


}
