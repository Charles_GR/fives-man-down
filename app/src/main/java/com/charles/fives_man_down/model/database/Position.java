package com.charles.fives_man_down.model.database;

public class Position
{

    private double latitude;
    private double longitude;

    public Position()
    {

    }

    public Position(double latitude, double longitude)
    {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude()
    {
        return latitude;
    }

    public double getLongitude()
    {
        return longitude;
    }

    public double calcDistanceTo(Position otherPosition)
    {
        final int RADIUS_OF_EARTH_MILES = 3959;
        double diffLat = Math.PI * (otherPosition.latitude - latitude) / 180;
        double diffLong = Math.PI * (otherPosition.longitude - longitude) / 180;
        double angle = Math.sin(diffLat / 2) * Math.sin(diffLat / 2) + Math.cos(Math.PI * latitude) * Math.cos(Math.PI *  otherPosition.latitude) *
                Math.sin(diffLong / 2) * Math.sin(diffLong / 2);
        double proportion = 2 * Math.atan2(Math.sqrt(angle), Math.sqrt(1 - angle));
        return Math.round(100d * proportion * RADIUS_OF_EARTH_MILES) / 100d;
    }

    @Override
    public String toString()
    {
        return "(" + Math.round(10f * latitude) / 10f + ", " + Math.round(10f * longitude) / 10f + ")";
    }
}
